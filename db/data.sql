-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2016 at 07:42 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pure_logics`
--

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `name`, `born_at`, `died_at`) VALUES
(1, ''Ahmad Faryab Kokab'', ''2011-01-01 00:00:00'', NULL);

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author_id`, `published_year`, `rack_id`, `created_on`, `modified_on`) VALUES
(1, ''Intoduction to Computers'', 1, ''2016-10-09 07:12:06'', 2, ''2016-10-09 15:14:35'', ''2016-10-09 16:31:55''),
(2, ''Introduction to Algorithum'', 1, ''2016-10-09 08:11:23'', 1, ''2016-10-09 16:20:35'', NULL),
(3, ''Introduction to Calculas'', 1, ''2016-10-09 08:21:31'', 1, ''2016-10-09 16:25:09'', NULL);

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `random_id`, `redirect_uris`, `secret`, `allowed_grant_types`) VALUES
(1, ''3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4'', ''a:0:{}'', ''4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k'', ''a:1:{i:0;s:8:"password";}'');

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `user_frequency_id`, `gender_id`, `type_id`, `first_name`, `last_name`, `dob`, `age`, `address_1`, `address_2`, `city`, `state`, `country`, `description`, `user_status`, `facebook_id`, `twitter_id`, `googleplus_id`, `postal_code`, `others`, `longitude`, `latitude`, `created_date`, `modified_date`, `profile_image`, `cover_image`, `updated_at`, `user_role_id`, `confirmation_code`) VALUES
(137, ''kokab@gmail.com'', ''kokab@gmail.com'', ''kokab@gmail.com'', ''kokab@gmail.com'', 1, ''4fav9a5stxc0wg8g0sw8s08w80oo8o8'', ''$2y$13$4fav9a5stxc0wg8g0sw8sujdfeCkOri9e09PnhRGvEMOFgB1nQNYu'', ''2016-10-09 18:47:34'', 0, 0, NULL, NULL, NULL, ''a:1:{i:0;s:10:"ROLE_USERS";}'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, ''admin@admin.com'', ''admin@admin.com'', ''admin@admin.com'', ''admin@admin.com'', 1, ''iqdx854szs0g8k0k8okkwo4kc040gs0'', ''$2y$13$iqdx854szs0g8k0k8okkweWioLgTkJLhm6RQJ89LwOi/SH//A9txS'', ''2016-10-09 20:38:07'', 0, 0, NULL, NULL, NULL, ''a:3:{i:0;s:10:"ROLE_USERS";i:1;s:10:"ROLE_ADMIN";i:2;s:16:"ROLE_SUPER_ADMIN";}'', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender_type`) VALUES
(1, ''male''),
(2, ''female''),
(3, ''note to say'');

--
-- Dumping data for table `racks`
--

INSERT INTO `racks` (`id`, `name`) VALUES
(1, ''Literature''),
(2, ''Science C-2'');

--
-- Dumping data for table `user_image`
--

INSERT INTO `user_image` (`id`, `name`) VALUES
(1, ''cover.png''),
(2, ''noprofile.jpg'');

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role_type`) VALUES
(5, ''ROLE_SUPER_ADMIN''),
(6, ''ROLE_USER''),
(7, ''ROLE_MANAGER'');

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `user_type`) VALUES
(1, ''web''),
(2, ''mobile''),
(5, ''facebook''),
(6, ''gplus''),
(7, ''Twitter''),
(9, ''Snapchat'');
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
