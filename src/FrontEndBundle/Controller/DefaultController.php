<?php

namespace FrontEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    public function isLoggedIn(){
        $user = $this->container->get('security.context')->getToken()->getUser();

        if(is_object($user)){
            return true;
        }else{
            return false;
        }
    }

    public function indexAction() {

        $router = $this->container->get('router');
        if($this->isLoggedIn()) {
            return new RedirectResponse($router->generate('acme_user_access_check'));
        }else{
            return $this->render('FrontEndBundle:Default:index.html.twig');
        }
    }


    public function listRacksAction(){
        $request = Request::createFromGlobals();
        $router = $this->container->get('router');
        if($this->isLoggedIn()) {

            $search_key = $request->get('likeKey',null);

            $rackCounts = [];

            $em = $this->getDoctrine()->getEntityManager();

            $queryString = 'SELECT r FROM BaseBundle:Racks r';

            if($search_key){

                $queryString .= ' WHERE r.name LIKE :likeKey';

            }

            $query = $em->createQuery($queryString);

            if($search_key){

                $query->setParameter('likeKey','%'.$search_key.'%');

            }

            $racks = $query->getResult();

            $queryString = 'SELECT COUNT(r) as count_of_racks, r as book FROM BaseBundle:Books r GROUP BY r.rack';

            $query = $em->createQuery($queryString);

            $result = $query->getResult();

            foreach ($result as $row){
                $book = $row['book'];
                $rackCounts[$book->getRack()->getId()] = $row['count_of_racks'];
            }

            return $this->render('FrontEndBundle:Racks:list.html.twig',[
                'racks' => $racks,
                'count' => $rackCounts,
                'searchKey' => $search_key
            ]);

        }else{
            return new RedirectResponse($router->generate('fron_end_homepage'));
        }

    }
    public function booksByIDAction($id){
        $router = $this->container->get('router');
        if($this->isLoggedIn()) {

            $rack = $this->getDoctrine()->getRepository('BaseBundle:Racks')->findById($id);
            $books = $this->getDoctrine()->getRepository('BaseBundle:Books')->findByRack($rack);
            return $this->render('FrontEndBundle:Books:list.html.twig',[
                'rack'=>$rack,
                'books'=>$books
            ]);

        }else{
            return new RedirectResponse($router->generate('fron_end_homepage'));
        }
    }

    public function booksAction(){
        $request = Request::createFromGlobals();
        $router = $this->container->get('router');
        if($this->isLoggedIn()) {

            $search_key = $request->get('likeKey',null);
            $em = $this->getDoctrine()->getEntityManager();

            $rackCounts = [];
            $authorsToSearch = [];

            if($search_key){

                $queryStringForAuth = 'SELECT a FROM BaseBundle:Author a WHERE a.name LIKE :likeKey';
                $queryForAuth = $em->createQuery($queryStringForAuth);
                $queryForAuth->setParameter('likeKey','%'.$search_key.'%');
                $authorsToSearch = $queryForAuth->getResult();
            }

            $queryString = 'SELECT b FROM BaseBundle:Books b';

            if($search_key){

                $queryString .= ' WHERE b.title LIKE :likeKey OR b.author IN(:authors)';

            }

            $query = $em->createQuery($queryString);

            if($search_key){

                $query->setParameter('likeKey','%'.$search_key.'%');
                $query->setParameter('authors',$authorsToSearch);

            }

            $books = $query->getResult();

//            $books = $this->getDoctrine()->getRepository('BaseBundle:Books')->findAll();
            return $this->render('FrontEndBundle:Books:list.html.twig',[
                'books'=>$books,
                'searchKey'=>$search_key
            ]);

        }else{
            return new RedirectResponse($router->generate('fron_end_homepage'));
        }
    }

    public function bookDetailAction($id){
        $router = $this->container->get('router');
        if($this->isLoggedIn()) {
            $book = $this->getDoctrine()->getRepository('BaseBundle:Books')->findOneById($id);
            return $this->render('FrontEndBundle:Books:info.html.twig',[
                'book'=>$book
            ]);

        }else{
            return new RedirectResponse($router->generate('fron_end_homepage'));
        }
    }

}
