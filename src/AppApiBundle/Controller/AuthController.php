<?php

namespace AppApiBundle\Controller;
use Facebook\Facebook;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BaseBundle\Entity;
require_once __DIR__ . './../../../vendor/facebook/php-sdk-v4/src/Facebook/autoload.php';

class AuthController extends DefaultController
{
    public function registerAction(Request $request)
    {

        $email      = $request->get('email');
        $password   = $request->get('password');
        $username   = $request->get('username');
        $first_name = $request->get('first_name');
        $age        = $request->get('age');
        $other      = $request->get('others');
        $latitude   = $request->get('latitude');
        $longitude  = $request->get('longitude');
        $address  = $request->get('address');
        $city  = $request->get('city');
        $state  = $request->get('state');
        $country  = $request->get('country');
        $postal_code  = $request->get('postal_code');

        $response = [];

        $error      = 0;
        $errordescription = [];
        if(!isset($username) || $username == ''){
            $username = $email;
        }
        if(!isset($email) || $email == ''){
            $error = 1;
            $errordescription[] = 'email missing';
        }
        if(!isset($password) || $password == ''){
            $error = 1;
            $errordescription[] = 'password missing';
        }
        if(!isset($address) || $address == ''){
            $error = 0;
            $errordescription[] = 'address missing';
        }
        if(!isset($city) || $city == ''){
            $error = 0;
            $errordescription[] = 'city missing';
        }
        if(!isset($state) || $state == ''){
            $error = 0;
            $errordescription[] = 'state missing';
        }
        if(!isset($country) || $country == ''){
            $error = 0;
            $errordescription[] = 'country missing';
        }
        if(!isset($postal_code) || $postal_code == ''){
            $error = 0;
            $errordescription[] = 'postal_code missing';
        }

        $user_manager = $this->get('fos_user.user_manager');

        if($error == 0) {

            $user_info_by_name = $user_manager->findUserByUsername($username);
            $user_info_by_mail = $user_manager->findUserByEmail($email);
            if($user_info_by_mail || $user_info_by_name){
                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                    $this->_RESPONSE_MSG_INDEX       => $this->_REGISTER_RESPONSE_MSG_INDEX,
                    $this->_RESPONSE_MSG_DESC_INDEX  => $this->_REGISTER_RESPONSE_DESC_INDEX,
                    $this->_USER_DATA_INDEX         => (object) []
                ]));
            }else{

                $user = new Entity\FosUser();
                $user->setUsername($username);
                $user->setEmail($email);
                $user->setPlainPassword($password);
                $user->setFirstName($first_name);
                $user->setOthers($other);
                $user->setAge($age);
                $user->setLatitude($latitude);
                $user->setLongitude($longitude);
                $user->setAddress($address);
                $user->setCity($city);
                $user->setState($state);
                $user->setCountry($country);
                $user->setPostalCode($postal_code);
                $user->setType($this->getDoctrine()->getRepository('BaseBundle:UserType')->findOneBy(['userType'=>'mobile']));

                $em = $this->getDoctrine()->getManager();

                // tells Doctrine you want to (eventually) save the Product (no queries yet)
                $em->persist($user);

                // actually executes the queries (i.e. the INSERT query)
                $em->flush();

                if($other){
                    $other_array = json_decode($other);
                    if($other_array) {
                        //Delete Previous Record

                        $userSportsDelArray = $this->getDoctrine()->getRepository('BaseBundle:UserSports')->findBy(['user'=>$user]);

                        if($userSportsDelArray){
                            foreach ($userSportsDelArray as $del){
                                $em = $this->getDoctrine()->getManager();
                                $em->remove($del);
                                $em->flush();
                            }
                        }

                        foreach ($other_array as $otherObj) {

                            $sport = $this->getDoctrine()->getRepository('BaseBundle:WsbSport')->findOneById($otherObj->sports_id);
                            if ($sport) {
                                $userSports = new Entity\UserSports();
                                $userSports->setLevel($otherObj->level);
                                $userSports->setSports($sport);
                                $userSports->setCanCoch($otherObj->is_coach);
                                $userSports->setCochAtClub(NULL);
                                $userSports->setUser($user);
                                $em = $this->getDoctrine()->getManager();
                                $em->persist($userSports);
                                $em->flush();
                            }
                        }
                    }
                }


                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX    => $this->_RESPONSE_STATUS_SUCCESS,
                    $this->_RESPONSE_MSG_INDEX       =>$this->_REGISTER_SUCCESS_MESSAGE_INDEX,
                    $this->_RESPONSE_MSG_DESC_INDEX  => $this->_REGISTER_SUCCESS_DESCRIPTION_INDEX,
                    $this->_USER_DATA_INDEX          => $this->make_user_object_by_obejct($user,$request)
                ]));
            }

        }else{
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX       => $this->_REGISTER_RESPONSE_MSG_REQUIRED_FEILDS,
                $this->_RESPONSE_MSG_DESC_INDEX  => $errordescription,
                $this->_USER_DATA_INDEX         => (object) []
            ]));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function editProfilePictureAction(Request $request){

        if($this->is_logged_in($request)){

            $image_type = 'user';
            $array = $this->upload_image($request, $image_type);

            if($array[0] = 1){

                $UserImages = new Entity\UserImage();
                $UserImages->setName($array[1]);

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($UserImages);
                $em->flush();

                $user = $this->get_user_by_id($this->get_user_id($request));

                $user->setProfileImage($UserImages);

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($user);
                $em->flush();

                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_SUCCESS,
                    $this->_RESPONSE_MSG_INDEX       => $this->_EDIT_PROFILE_PICTURE_SUCCESS_MESSAGE_INDEX,
                    $this->_RESPONSE_MSG_DESC_INDEX  => $this->_EDIT_PROFILE_PICTURE_SUCCESS_DESCRIPTION_INDEX,
                    $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                    $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                ]));

            }else{
                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                    $this->_RESPONSE_MSG_INDEX       => $this->_EDIT_PROFILE_PICTURE_MSG_INDEX,
                    $this->_RESPONSE_MSG_DESC_INDEX  => $this->_EDIT_PROFILE_PICTURE_DESC_INDEX,
                    $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                    $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                ]));
            }
        }else{
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX       => $this->_ALL_LOGIN_INDEX,
                $this->_RESPONSE_MSG_DESC_INDEX  => $this->_LOGIN_DESCRIPTION_INDEX,
                $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
            ]));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    public function editProfileAction(Request $request)
    {

        if($this->is_logged_in($request)){

            $user = $this->get_user_by_id($this->get_user_id($request));

            if($user){

                /**
                 * The Only Edit Able Parameters
                 * Make Sure No other Stub is Going To Update
                 */

                $firstName      = $request->get('first_name',null);
                $lastName       = $request->get('last_name',null);
                $age            = $request->get('age',null);
                $gender         = $request->get('gender',null);
                $description    = $request->get('description', null);
                $city           = $request->get('city', null);
                $others         = $request->get('others', null);

                if($firstName){
                    $user->setFirstName($firstName);
                }

                if($lastName){
                    $user->setFirstName($lastName);
                }

                if($age){
                    $user->setAge($age);
                }

                if($gender){
                    $genderObject = $this->getDoctrine()->getRepository('BaseBundle:Gender')->findOneBy(array('genderType'=>$gender));
                    if($genderObject){
                        $user->setGender($genderObject);
                    }
                }

                if($description){
                    $user->setDescription($description);
                }

                if($city){
                    $user->setCity($city);
                }

                if($others){
                    $user->setOthers($others);
                }

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($user);
                $em->flush();

                if($others){
                    $other_array = json_decode($others);
                    if($other_array) {
                        //Delete Previous Record

                        $userSportsDelArray = $this->getDoctrine()->getRepository('BaseBundle:UserSports')->findBy(['user'=>$user]);

                        if($userSportsDelArray){
                            foreach ($userSportsDelArray as $del){
                                $em = $this->getDoctrine()->getManager();
                                $em->remove($del);
                                $em->flush();
                            }
                        }

                        foreach ($other_array as $otherObj) {

                            $sport = $this->getDoctrine()->getRepository('BaseBundle:WsbSport')->findOneById($otherObj->sports_id);
                            if ($sport) {
                                $userSports = new Entity\UserSports();
                                $userSports->setLevel($otherObj->level);
                                $userSports->setSports($sport);
                                $userSports->setCanCoch($otherObj->is_coach);
                                $userSports->setCochAtClub(NULL);
                                $userSports->setUser($user);
                                $em = $this->getDoctrine()->getManager();
                                $em->persist($userSports);
                                $em->flush();
                            }
                        }
                    }
                }

                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_SUCCESS,
                    $this->_RESPONSE_MSG_INDEX       => $this->_EDIT_PROFILE_SUCCESS_MESSAGE_INDEX,
                    $this->_RESPONSE_MSG_DESC_INDEX  => $this->_EDIT_PROFILE_SUCCESS_DESCRIPTION_INDEX,
                    $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                    $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                ]));

            }else{
                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                    $this->_RESPONSE_MSG_INDEX       => $this->_EDIT_PROFILE_MSG_INDEX,
                    $this->_RESPONSE_MSG_DESC_INDEX  => $this->_EDIT_PROFILE_DESC_INDEX,
                    $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                    $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                ]));
            }
        }else{
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX       => $this->_ALL_LOGIN_INDEX,
                $this->_RESPONSE_MSG_DESC_INDEX  => $this->_LOGIN_DESCRIPTION_INDEX,
                $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
            ]));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
   }
   
    public function updatePasswordAction(Request $request){
        if($this->is_logged_in($request)){
       //Intializing Response Variables
        

        //retriving Required data
        $password=$request->get('password');
        $newPassword=$request->get('new_password');
        $uid = $this->get_user_id($request);
        
            //Getting Entity Manager
            $em = $this->getDoctrine()->getEntityManager();
            //Getting User Repository
            $userRepo = $em->getRepository('BaseBundle:FosUser');

            //Getting Data From User Repository
            $user_info= $userRepo->findOneById($uid);
            $user = $this->get_user_by_id($this->get_user_id($request));
            
            $encoder_service = $this->get('security.encoder_factory');
            $encoder = $encoder_service->getEncoder($user);
            $encoded_pass = $encoder->encodePassword($password, $user->getSalt());
            //If User Info is Available
            if($user_info){
                
                if($password){
                 if($encoded_pass == $user_info->getPassword()){ 
                    
                 
                 $user_info->setPassword($encoder->encodePassword($newPassword, $user->getSalt()));
//                 echo $user_info->getPlainPassword();
                 $user_info->setEnabled(1);
                 $em->persist($user_info);
                 $em->flush();
                 $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_SUCCESS,
                        $this->_RESPONSE_MSG_INDEX      => $this->_UPDATE_PASSWORD_SUCCESS_MESSAGE_INDEX,
                        $this->_RESPONSE_MSG_DESC_INDEX  =>$this->_UPDATE_PASSWORD_SUCCESS_DESCRIPTION_INDEX,
                    ]));
                     }else{
                      $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                         $this->_RESPONSE_MSG_INDEX       => $this->_UPDATE_PASSWORD_MSG_INDEX,
                         $this->_RESPONSE_MSG_DESC_INDEX  =>  $this->_UPDATE_PASSWORD_DESC_INDEX ,
                    ]));
                }
            }else{
                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                        $this->_RESPONSE_MSG_INDEX       => $this->_UPDATE_PASSWORD_MESSAGE_INDEX,
                        $this->_RESPONSE_MSG_DESC_INDEX  => $this->_UPDATE_PASSWORD_DESCRIPTION_INDEX,
                    ]));
                }
            }else{
                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                        $this->_RESPONSE_MSG_INDEX       => $this->_UPDATE_PASSWORD_MESSAGE_ID_INDEX,
                        $this->_RESPONSE_MSG_DESC_INDEX  => $this->_UPDATE_PASSWORD_DESCRIPTION_ID_INDEX,
                    ]));
                }
           
        }else{
          $response = new Response(json_encode([
              $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
              $this->_RESPONSE_MSG_INDEX       => $this->_ALL_LOGIN_INDEX,
              $this->_RESPONSE_MSG_DESC_INDEX  => $this->_LOGIN_DESCRIPTION_INDEX,
          ]));
      }
       $response->headers->set('Content-Type', 'application/json');
        return $response;
   }

    public function getUserProfileInfoAction(Request $request){
        if($this->is_logged_in($request)){

            $uid    =   $this->get_user_id($request);
            $fid    =   $request->get('user_id',0);

            $userProfile = $this->get_user_by_id($uid);
            $buddyProfile = $this->get_user_by_id($fid);

            if($userProfile || $buddyProfile){

                if($buddyProfile) {
                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX       =>  $this->_RESPONSE_STATUS_SUCCESS,
                        $this->_RESPONSE_MSG_INDEX          => $this->_GET_USER_PROFILE_SUCCESS_MESSAGE_INDEX,
                        $this->_RESPONSE_MSG_DESC_INDEX     =>  $this->_GET_USER_PROFILE_SUCCESS_DESCRIPTION_INDEX,
                        $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($userProfile,$request),
                        $this->_OTHER_USER_DATA_LIST_INDEX  =>  $this->make_user_detail_object_by_obejct($buddyProfile,$request),
                        $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                    ]));
                }else{
                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX       =>  $this->_RESPONSE_STATUS_SUCCESS,
                        $this->_RESPONSE_MSG_INDEX          =>  $this->_GET_USER_PROFILE_SUCCESS_MESSAGE_INDEX,
                        $this->_RESPONSE_MSG_DESC_INDEX     =>  $this->_GET_USER_PROFILE_SUCCESS_DESCRIPTION_INDEX,
                        $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($userProfile,$request),
                        $this->_OTHER_USER_DATA_LIST_INDEX  =>  (object) [],
                        $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                    ]));
                }

            }else{
                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX       =>  $this->_RESPONSE_STATUS_FAILED,
                    $this->_RESPONSE_MSG_INDEX          =>  $this->_GET_USER_PROFILE_MSG_INDEX,
                    $this->_RESPONSE_MSG_DESC_INDEX     =>   $this->_GET_USER_PROFILE_DESC_INDEX ,
                    $this->_USER_DATA_INDEX             =>  (object) [],
                    $this->_OTHER_USER_DATA_LIST_INDEX  =>  (object) [],
                    $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                ]));
            }
        }else{
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX       => $this->_ALL_LOGIN_INDEX,
                $this->_RESPONSE_MSG_DESC_INDEX  => $this->_LOGIN_DESCRIPTION_INDEX,
                $this->_USER_DATA_INDEX             =>  (object) [],
                $this->_OTHER_USER_DATA_LIST_INDEX  =>  (object) [],
                $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
            ]));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getFacebookLoginAction(Request $request)
    {
        $fb_id      = $request->get('access_token');

        $fb = new Facebook([
            'app_id' => '1116167461788698',
            'app_secret' => '045ee3fd5269e8a658d08cbb765bb67e',
            'default_graph_version' => 'v2.6',
//            'default_access_token' => $fb_id,
        ]);


        $response = $fb->get('/me?fields=id,name,email', $fb_id);
        $me = $response->getGraphUser();

        $email = $me->getEmail();

        $user = $this->getDoctrine()->getRepository('BaseBundle:FosUser')->findOneByFacebookId($me->getId());

        if($user){

            //Login Method

        }else{
            $user = $this->getDoctrine()->getRepository('BaseBundle:FosUser')->findOneByEmail($me->getEmail());

            if($user){
                $user->setFacebookId($me->getId());
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($user);
                $em->flush();
            }else{

                $user = new Entity\FosUser();
                $user->setUsername($this->clean($me->getName()));
                $user->setPassword($me->getId());
                $user->setEmail($me->getEmail());
                $user->setFacebookId($me->getId());

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($user);
                $em->flush();
            }
        }

        return $this->log_me_in_using_email($email);
    }

    private function log_me_in_using_email($email){

        $user = $this->getDoctrine()->getRepository('BaseBundle:FosUser')->findOneByEmail($email);

        if($user){

            $client = $this->getDoctrine()->getRepository('BaseBundle:Client')->findOneById(1);

            $accessTokenString = $this->genAccessToken();

            $accessToken = new Entity\AccessToken();

            $accessToken->setUser($user);
            $accessToken->setClient($client);
            $accessToken->setToken($accessTokenString);
            $accessToken->setScope(NULL);

            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($accessToken);
            $em->flush();

            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_SUCCESS,
                'access_token'      =>  $accessTokenString,
                $this->_RESPONSE_MSG_INDEX       => $this->_GET_FACEBOOK_PROFILE_SUCCESS_MESSAGE_INDEX,
                $this->_RESPONSE_MSG_DESC_INDEX  =>$this->_GET_FACEBOOK_PROFILE_SUCCESS_DESCRIPTION_INDEX,
            ]));

        }else{
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX       => $this->_GET_FACEBOOK_PROFILE_MSG_INDEX,
                $this->_RESPONSE_MSG_DESC_INDEX  => $this->_GET_FACEBOOK_PROFILE_DESC_INDEX,
            ]));
        }
        return $response;
    }

    private function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    protected function genAccessToken()
    {
        $num = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);

        return sha1($num);
    }

    public function getUsersAction(Request $request){

        if($this->is_logged_in($request)){
        

            $user = $this->get_user_by_id($this->get_user_id($request));

            $userSearch = $request->get('search_key','');
            $latitude   =   $request->get('latitude',0);
            $longitude  =   $request->get('longitude',0);
            $distance   =   $request->get('distance',100);
            $clubId   =   $request->get('club_id',null);
            $sportsId   =   $request->get('sports_id',null);
            $page = $request->get('page_number', 0);
            $count = 10;
            $start = $page * $count;

            $end = $count;
            $users = [];

            $clubUsers = [];
            if($clubId) {
                $club = $this->getDoctrine()->getRepository('BaseBundle:WsbClub')->findOneById($clubId);

                if ($club) {
                    $FollowclubUsers = $this->getDoctrine()->getRepository('BaseBundle:ClubFollowers')->findByClub($club);
                    foreach ($FollowclubUsers as $u) {
                        $clubUsers[] = $u->getUser()->getId();
                    }
                    $FavoriteclubUsers = $this->getDoctrine()->getRepository('BaseBundle:ClubFavorite')->findByClub($club);
                    foreach ($FavoriteclubUsers as $u) {
                        $clubUsers[] = $u->getUser()->getId();
                    }
                }
            }
            if($sportsId) {
                $sports = $this->getDoctrine()->getRepository('BaseBundle:WsbSport')->findOneById($sportsId);

                $userSports = $this->getDoctrine()->getRepository('BaseBundle:UserSports')->findBySports($sports);

                foreach ($userSports as $u){
                    $clubUsers[] = $u->getUser()->getId();
                }

            }

            $em = $this->getDoctrine()->getManager();

            if($latitude & $longitude) {

                $em = $this->getDoctrine()->getManager();

                $queryString = 'SELECT e as userObject, SQRT(POWER((69.1 * (e.latitude - :startlat)), 2) + POWER(((69.1 * (:startlng - e.longitude)) * ( COS(DIVIDE(e.latitude, 57.3)))) , 2) AS distance FROM BaseBundle:FosUser e ';

                $queryString .= ' WHERE e.roles LIKE :role ';

                if($clubId || $sportsId){
                    $queryString .= ' AND e.id IN (:userIds) ';
                }

                if ($userSearch != '') {

                    $queryString .= ' AND e.username LIKE :user OR e.firstName LIKE :user OR e.lastName LIKE :user ';
                }

                $query = $em->createQuery($queryString);
                $query->setParameter('startlat',$latitude);
                $query->setParameter('startlng',$longitude);
                $query->setParameter('role','%"'.'ROLE_USERS'.'"%');

                if($clubId || $sportsId){
                    $query->setParameter('userIds',$clubUsers);
                }
                if ($userSearch != '') {
                    $query->setParameter('user', '%' . $userSearch . '%');
                }
                $result = $query->getResult();

                foreach ($result as $r){
                    $users[] = $r['userObject'];
                }

            }else {

                $queryString = 'SELECT u FROM BaseBundle:FosUser u';
                $queryString .= ' WHERE u.roles LIKE :role ';
                if($clubId || $sportsId){
                    $queryString .= ' AND u.id IN (:userIds) ';
                }
                if ($userSearch != '') {

                    $queryString .= ' AND u.username LIKE :user OR u.firstName LIKE :user OR u.lastName LIKE :user ';

                    $qb = $em->createQuery($queryString);
                    if($clubId || $sportsId){
                        $qb->setParameter('userIds',$clubUsers);
                    }
                    $qb->setParameter('user', '%' . $userSearch . '%')->setParameter('role','%"'.'ROLE_USERS'.'"%')
                        ->getResult();

                    $users = $qb->getResult();

                } else {

                    $query = $em->createQuery($queryString);

                    $query->setFirstResult($start)->setMaxResults($end);
                    $query->setParameter('role','%"'.'ROLE_USERS'.'"%');
                    if($clubId || $sportsId){
                        $query->setParameter('userIds',$clubUsers);
                    }

                    $users = $query->getResult();

                }
            }

            if($users) {

                $blockedList = $this->getDoctrine()->getRepository('BaseBundle:BlockedUsers')->findBy(['blockedBy'=> $user]);

                $blockedIds = $this->get_restricted_users([$this->get_user_id($request)]);

                foreach ($blockedList as $blocked){
                    $blockedIds[] = $blocked->getUserId();
                }

                foreach ($users as $key=>$u){
                    if(in_array($u->getId(),$blockedIds)){
                        unset($users[$key]);
                    }
                }

                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX => $this->_RESPONSE_STATUS_SUCCESS,
                    $this->_RESPONSE_MSG_INDEX => $this->_GEN_ACCESS_TOKEN_SUCCESS_MESSAGE_INDEX,
                    $this->_RESPONSE_MSG_DESC_INDEX => $this->_GEN_ACCESS_TOKEN_SUCCESS_DESCRIPTION_INDEX ,
                    $this->_USERS_ARRAY_INDEX => $this->make_user_object_by_array_of_objects($users,$request)
                ]));
            }else{
                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                    $this->_RESPONSE_MSG_INDEX       =>$this->_GEN_ACCESS_TOKEN_MSG_INDEX,
                    $this->_RESPONSE_MSG_DESC_INDEX  =>$this->_GEN_ACCESS_TOKEN_DESC_INDEX,
                ]));
            }
        }else{
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX       => $this->_ALL_LOGIN_INDEX,
                $this->_RESPONSE_MSG_DESC_INDEX  => $this->_LOGIN_DESCRIPTION_INDEX,
            ]));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    public function markAvailabilityAction(Request $request){
        if($this->is_logged_in($request)){

            $dayId = $request->get('day',0);
            $timeId = $request->get('time',0);
            $status = $request->get('status',0);
            $availability=$request->get('availability',0);

            if($dayId != 0 && $timeId != 0){

                $day = $this->getDoctrine()->getRepository('BaseBundle:AvailabilityDays')->findOneById($dayId);
                $time = $this->getDoctrine()->getRepository('BaseBundle:AvailabilityTimming')->findOneById($timeId);

                if($day && $time && ($status == 1 || $status == 0)){

                    $userTimming = $this->getDoctrine()->getRepository('BaseBundle:UserAvailability')->findOneBy(
                        array(
                            'user'=>$this->get_user_by_id($this->get_user_id($request)),
                            'time'=>$time,
                            'day'=>$day
                        )
                    );

                    if($userTimming){
                        $userTimming->setStatus($status);

                        $em = $this->getDoctrine()->getEntityManager();
                        $em->persist($userTimming);
                        $em->flush();
                    }else{
                        $userTimming = new Entity\UserAvailability();
                        $userTimming->setDay($day);
                        $userTimming->setStatus($status);
                        $userTimming->setUser($this->get_user_by_id($this->get_user_id($request)));
                        $userTimming->setTime($time);

                        $em = $this->getDoctrine()->getEntityManager();
                        $em->persist($userTimming);
                        $em->flush();
                    }

                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX       =>  $this->_RESPONSE_STATUS_SUCCESS,
                        $this->_RESPONSE_MSG_INDEX          =>  'user availability status updated',
                        $this->_RESPONSE_MSG_DESC_INDEX     =>  ["user availability status updated."],
                        $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                        $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                    ]));

                }else{
                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                        $this->_RESPONSE_MSG_INDEX       => 'day, status or time is invalid',
                        $this->_RESPONSE_MSG_DESC_INDEX  => ["day, status or time is invalid."],
                        $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                        $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                    ]));
                }

            }else{
                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                    $this->_RESPONSE_MSG_INDEX       => 'day, time or status is missing',
                    $this->_RESPONSE_MSG_DESC_INDEX  => ["required data is missing."],
                    $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                    $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                ]));
            }
        }else{
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX       => 'access denied',
                $this->_RESPONSE_MSG_DESC_INDEX  => ["please login first."],
                $this->_USER_DATA_INDEX             =>  (object) [],
                $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
            ]));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    public function getMarkAvailabilityAction(Request $request){
       
        if($this->is_logged_in($request)){
            $em = $this->getDoctrine()->getEntityManager();
            $status_info=$userTimming = $em->getRepository('BaseBundle:UserAvailability')->findAll();
            foreach($status_info as $info){
                $info->setStatus(0);
                $em->flush();        
            }
            
            $availability=$request->get('availability',0);
            $object = json_decode($availability);
            foreach($object as $obj){
                $dayId=$obj->day;
                $timeId=$obj->time;
                $status=$obj->status;
                
            if($dayId != 0 && $timeId != 0){

                $day = $this->getDoctrine()->getRepository('BaseBundle:AvailabilityDays')->findOneById($dayId);
                $time = $this->getDoctrine()->getRepository('BaseBundle:AvailabilityTimming')->findOneById($timeId);

                if($day && $time && ($status == 1 || $status == 0)){

                    $userTimming = $this->getDoctrine()->getRepository('BaseBundle:UserAvailability')->findOneBy(
                        array(
                            'user'=>$this->get_user_by_id($this->get_user_id($request)),
                            'time'=>$time,
                            'day'=>$day
                        )
                    );

                    if($userTimming){
                        $userTimming->setStatus($status);

                        $em = $this->getDoctrine()->getEntityManager();
                        $em->persist($userTimming);
                        $em->flush();
                    }else{
                        $userTimming = new Entity\UserAvailability();
                        $userTimming->setDay($day);
                        $userTimming->setStatus($status);
                        $userTimming->setUser($this->get_user_by_id($this->get_user_id($request)));
                        $userTimming->setTime($time);

                        $em = $this->getDoctrine()->getEntityManager();
                        $em->persist($userTimming);
                        $em->flush();
                    }

                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX       =>  $this->_RESPONSE_STATUS_SUCCESS,
                        $this->_RESPONSE_MSG_INDEX          =>  'user availability status updated',
                        $this->_RESPONSE_MSG_DESC_INDEX     =>  ["user availability status updated."],
                        $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                        $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                    ]));

                }else{
                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                        $this->_RESPONSE_MSG_INDEX       => 'day, status or time is invalid',
                        $this->_RESPONSE_MSG_DESC_INDEX  => ["day, status or time is invalid."],
                        $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                        $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                    ]));
                }

            }else{
                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                    $this->_RESPONSE_MSG_INDEX       => 'day, time or status is missing',
                    $this->_RESPONSE_MSG_DESC_INDEX  => ["required data is missing."],
                    $this->_USER_DATA_INDEX             =>  $this->make_user_detail_object_by_obejct($this->get_user_by_id($this->get_user_id($request)),$request),
                    $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
                ]));
            }
            }
        }else{
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX       => 'access denied',
                $this->_RESPONSE_MSG_DESC_INDEX  => ["please login first."],
                $this->_USER_DATA_INDEX             =>  (object) [],
                $this->_EXTRA_INFO_INDEX            => $this->get_days_timings()
            ]));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function forgetPasswordAction(Request $request){
         
        if($this->is_logged_in($request)){
         
        }else{
            $response = new Response(json_encode([
             $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX       => 'access denied',
                $this->_RESPONSE_MSG_DESC_INDEX  => ["please login first."], 
                ]));
        }
        
    }

    public function addDevicesAction(Request $request){
        
        if($this->is_logged_in($request)){
            
      
          $device_id = $request->get('device_id'); 
          $user = $this->get_user_by_id($this->get_user_id($request));
          $user_device_info = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:UserDevicesList')->findOneBy(['user'=>$user,'deviceId'=>$device_id]);
          if($user_device_info){
                
                $response = new Response(json_encode([
                  $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_SUCCESS,
                  $this->_RESPONSE_MSG_INDEX       => 'deveice assign to new user of previous user',
                  $this->_RESPONSE_MSG_DESC_INDEX  => ["deveice assigned successfully."],
                ]));
          }else{
                $user_device_info1 = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:UserDevicesList')->findOneBy(['deviceId'=>$device_id]);
                if($user_device_info1){
                    $em=$this->getDoctrine()->getEntityManager();
                    $em->remove($user_device_info1);
                    $em->flush();

                    $deviceList = new Entity\UserDevicesList();
                    $deviceList->setDeviceType("Electronics");
                    $deviceList->setDeviceId($device_id);
                    $deviceList->setAditional("Laptop");
                    $deviceList->setStatus(1);
                    $deviceList->setCreatedOn(new \DateTime());
                    $deviceList->setUser($user);
                    $em=$this->getDoctrine()->getEntityManager();
                    $em->persist($deviceList);
                    $em->flush();

                    $response = new Response(json_encode([
                      $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_SUCCESS,
                      $this->_RESPONSE_MSG_INDEX       => 'deveice assign to new user of previous user',
                      $this->_RESPONSE_MSG_DESC_INDEX  => ["deveice assigned successfully."],
                    ]));
              }else{
                    $deviceList = new Entity\UserDevicesList();
                    $deviceList->setDeviceType("Electronics");
                    $deviceList->setDeviceId($device_id);
                    $deviceList->setAditional("Laptop");
                    $deviceList->setStatus(1);
                    $deviceList->setCreatedOn(new \DateTime());
                    $deviceList->setUser($user);
                    $em=$this->getDoctrine()->getEntityManager();
                    $em->persist($deviceList);
                    $em->flush();

                     $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_SUCCESS,
                        $this->_RESPONSE_MSG_INDEX       => 'deveice assign to the current user',
                        $this->_RESPONSE_MSG_DESC_INDEX  => ["deveice assign to the current user."],
                    ]));
              }   
          }
     }else{
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX   => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX       => 'access denied',
                $this->_RESPONSE_MSG_DESC_INDEX  => ["please login first."],
               
            ]));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getBlockedUsersAction(Request $request){
        if ($this->is_logged_in($request)) {

            $user = $this->get_user_id($request);

            if($user) {

                $usersBlockedRecord = $this->getDoctrine()->getRepository('BaseBundle:BlockedUsers')->findBy(['blockedBy' => $user]);

                if($usersBlockedRecord){

                    $users = [];

                    foreach ($usersBlockedRecord as $blockRecord) {

                        $users[] = $this->get_user_by_id($blockRecord->getUserId());
                    }
                    if($users) {
                        $response = new Response(json_encode([
                            $this->_RESPONSE_STATUS_INDEX => $this->_RESPONSE_STATUS_SUCCESS,
                            $this->_RESPONSE_MSG_INDEX => "blocked users list found",
                            $this->_RESPONSE_MSG_DESC_INDEX => ["blocked users list found"],
                            $this->_USERS_ARRAY_INDEX => $this->make_user_object_by_array_of_objects($users,$request)
                        ]));
                    }else{
                        $response = new Response(json_encode([
                            $this->_RESPONSE_STATUS_INDEX => $this->_RESPONSE_STATUS_FAILED,
                            $this->_RESPONSE_MSG_INDEX => "you haven't block any user",
                            $this->_RESPONSE_MSG_DESC_INDEX => ["you haven't block any user"],
                            $this->_USERS_ARRAY_INDEX => []
                        ]));
                    }
                }else{
                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX => $this->_RESPONSE_STATUS_FAILED,
                        $this->_RESPONSE_MSG_INDEX => "you haven't block any user",
                        $this->_RESPONSE_MSG_DESC_INDEX => ["you haven't block any user"],
                        $this->_USERS_ARRAY_INDEX => []
                    ]));
                }

            }
        } else {
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX => $this->_ALL_LOGIN_INDEX,
                $this->_RESPONSE_MSG_DESC_INDEX => $this->_LOGIN_DESCRIPTION_INDEX,
                $this->_USERS_ARRAY_INDEX => []
            ]));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function unblockFriendAction(Request $request) {
        if ($this->is_logged_in($request)) {

            $uid = $this->get_user_id($request);
            $bid = $request->get('blocked_user_id');

            //If Required adata is valid
            $isValidUserId = $uid != 0 && $uid != '' && $uid != NULL && $uid;
            $isValidBlockId = $bid != 0 && $bid != '' && $bid != NULL && $bid;

            if ($isValidUserId && $isValidBlockId) {

                $blockuser = $this->get_user_by_id($bid);

                if ($blockuser) {

                    $em = $this->getDoctrine()->getEntityManager();

                    $blockedObjects = $this->getDoctrine()->getRepository('BaseBundle:BlockedUsers')->findBy(['userId'=>$bid,'blockedBy'=>$uid]);

                    foreach ($blockedObjects as $blockedObject){
                        $em->remove($blockedObject);
                    }


                    $em->flush();


                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX => $this->_RESPONSE_STATUS_SUCCESS,
                        $this->_RESPONSE_MSG_INDEX => "user unblocked",
                        $this->_RESPONSE_MSG_DESC_INDEX => [ "user unblocked"]
                    ]));
                } else {
                    $response = new Response(json_encode([
                        $this->_RESPONSE_STATUS_INDEX => $this->_RESPONSE_STATUS_FAILED,
                        $this->_RESPONSE_MSG_INDEX => $this->_BLOCK_FRIEND_FAIL_MSG_INDEX,
                        $this->_RESPONSE_MSG_DESC_INDEX => $this->_BLOCK_FRIEND_FAIL_DESC_INDEX,
                    ]));
                }
            } else {
                $response = new Response(json_encode([
                    $this->_RESPONSE_STATUS_INDEX => $this->_RESPONSE_STATUS_FAILED,
                    $this->_RESPONSE_MSG_INDEX => $this->_BLOCK_FRIEND_RESPONSE_MSG_INDEX,
                    $this->_RESPONSE_MSG_DESC_INDEX => $this->_BLOCK_FRIEND_RESPONSE_DESC_INDEX,
                ]));
            }
        } else {
            $response = new Response(json_encode([
                $this->_RESPONSE_STATUS_INDEX => $this->_RESPONSE_STATUS_FAILED,
                $this->_RESPONSE_MSG_INDEX => $this->_ALL_LOGIN_INDEX,
                $this->_RESPONSE_MSG_DESC_INDEX => $this->_LOGIN_DESCRIPTION_INDEX,
            ]));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}