<?php

namespace AppApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use BaseBundle\Entity\FosUser;
use FOS\RestBundle\Controller\FOSRestController;
use BaseBundle\Entity;

class DefaultController extends FOSRestController {

    protected function is_logged_in(Request $request) {

        if ($this->_SESSION_KEY == '') {
            $this->_SESSION_KEY = $request->get($this->_SESSION_KEY_INDEX);
        }
        $token = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:AccessToken')->findOneByToken($this->_SESSION_KEY);

        if (is_object($token) && $token->getUser()->getId()) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    protected function default_image(Request $request) {
        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/no-image.png';
        $return = (object) [
                    'id' => 1,
                    'path' => $base_path,
        ];
        return $return;
    }
    protected function default_user_image(Request $request) {
        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/user/no-image.png';
        $return = (object) [
            'id' => 1,
            'path' => $base_path,
        ];
        return $return;
    }

    protected function get_user_id(Request $request) {
        if ($this->_SESSION_KEY == '') {
            $this->_SESSION_KEY = $request->get($this->_SESSION_KEY_INDEX);
        }

        $token = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:AccessToken')->findOneByToken($this->_SESSION_KEY);

        if ($user_id = $token->getUser()->getId()) {

            return $user_id;
        } else {

            return FALSE;
        }
    }

    protected function get_user_by_id($id) {


        $user = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:FosUser')->findOneById($id);

        if ($user) {

            return $user;
        } else {

            return FALSE;
        }
    }

    protected function make_message_object_by_object(Entity\ThreadMessages $message, Request $request) {

        $messageResponse = (object) [
                    'sender_read' => $message->getSenderRead() == true || $message->getSenderRead() == 1 ? true : false,
                    'receiver_read' => $message->getReceavedRead() == true || $message->getReceavedRead() == 1 ? true :false,
                    'sender_status' => $message->getSenderStatus()== true || $message->getSenderStatus()== 1 ? true : false ,
                    'receiver_status' => $message->getReceaverStatus() == true || $message->getReceaverStatus() == 1 ? true : false,
                    'message' => $message->getMessage(),
                    'created_date' => $message->getCreatedOn(),
                    'modified_date' => $message->getModifiedOn(),
                    'thread' => $message->getThread()->getId(),
                    'sender_user' => $this->make_user_object_by_obejct($message->getSender(), $request),
                    'receiver_user' => $this->make_user_object_by_obejct($message->getReceaver(), $request),
                     'sender_id' => $message->getSender()->getId()
        ];
        return $messageResponse;
    }

    protected function make_message_object_by_array($messages, Request $request) {
        $return = [];
        foreach ($messages as $message) {
            $return[] = $this->make_message_object_by_object($message, $request);
        }
        return $return;
    }
   
    protected function make_thread_object_by_object(Entity\Threads $thread, Request $request) {
        
        $user_info1 = $this->get_user_by_id($this->get_user_id($request));
        $thread_info = [];
            $thread_info1 = $this->getDoctrine()->getRepository('BaseBundle:Threads')->findByUser1($user_info1);
            $thread_info2 = $this->getDoctrine()->getRepository('BaseBundle:Threads')->findByUser2($user_info1);

            if ($thread_info1 && $thread_info2) {
                $thread_info = array_merge($thread_info1, $thread_info2);
            } else {

                if ($thread_info1) {
                    $thread_info = $thread_info1;
                } elseif ($thread_info2) {

                    $thread_info = $thread_info2;
                }
            }
            $unread_msg = $this->getDoctrine()->getRepository('BaseBundle:ThreadMessages')->findBy(array('receaver'=>$user_info1,'receavedRead'=>0,'thread'=>$thread_info));
            $unread= count($unread_msg);
        
        $last_message="";
          $msg_info = $this->getDoctrine()->getRepository('BaseBundle:ThreadMessages')->findByThread($thread);
        foreach($msg_info as $msg){
                 $last_message= $msg->getMessage();
                
         }
        
        
        $threadResponse = (object) [
                    'id' => $thread->getId(),
                    'name' => $thread->getName(),
                    'created_on' => $thread->getCreatedOn(),
                    'modified_on' => $thread->getModifiedOn(),
                    
                    'club_info' => $thread->getClub() ? $this->make_club_detail_object_by_object($thread->getClub(), $request): (object)[],
                    'event_info' => $thread->getEvent()?$this->make_event_object_by_object($thread->getEvent(), $request): (object)[],
                    'user_info' => $this->make_user_object_by_obejct($thread->getUser1(), $request),
                    'other_user_info' => $this->make_user_object_by_obejct($thread->getUser2(), $request),
                    'last_message' => $last_message,
                    'unread_messages' => $unread
        ];
        return $threadResponse;
    }

    protected function make_thread_object_by_array($thread_info, Request $request) {
        
        $return = [];
        foreach ($thread_info as $thread) {
            $return[] = $this->make_thread_object_by_object($thread, $request);
        }
        return $return;
    }

    protected function get_images_ids($id = []) {


        $images = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:EventImages')->findAllById($id);

        if ($images) {

            return $images;
        } else {

            return FALSE;
        }
    }

    protected function get_images_byevent_id($id) {


        $images = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:EventImages')->findOneById($id);

        if ($images) {

            return $images;
        } else {

            return FALSE;
        }
    }

    protected function is_attending_event_object($event, $request) {

        $user = $this->get_user_by_id($this->get_user_id($request));
        $event_user_status = [
            'invited' => 0,
            'attending' => 0,
            'canceled' => 0,
            'maybe_attending' => 0
        ];
        $EventInvitation = $this->getDoctrine()->getRepository('BaseBundle:EventInvitations')->findOneBy([
            'event' => $event,
            'toUser' => $user
        ]);
        if ($EventInvitation) {
            $event_user_status['invited'] = 1;
        }
        $AttendingEvent = $this->getDoctrine()->getRepository('BaseBundle:EventPartcipant')->findOneBy([
            'event' => $event,
            'user' => $user,
            'status' => 1
        ]);

        if ($AttendingEvent) {
            $event_user_status['attending'] = 1;
        }

        $AttendingEvent = $this->getDoctrine()->getRepository('BaseBundle:EventPartcipant')->findOneBy([
            'event' => $event,
            'user' => $user,
            'status' => 0
        ]);

        if ($AttendingEvent) {
            $event_user_status['canceled'] = 1;
        }
        
          $maybeAttendingEvent = $this->getDoctrine()->getRepository('BaseBundle:EventPartcipant')->findOneBy([
            'event' => $event,
            'user' => $user,
            'status' => 2
        ]);

        if ($maybeAttendingEvent) {
            $event_user_status['maybe_attending'] = 1;
        }

        return (object) $event_user_status;
    }

    protected function parse_user_image_object(Entity\UserImage $image, Request $request) {

        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/';

        $return = (object) [
                    'id' => $image->getId(),
                    'path' => $base_path . (($image->getName()) ? $image->getName() : 'logo1.png' ),
        ];

        return $return;
    }

    protected function parse_club_image_object(Entity\ClubImages $image, Request $request) {

        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/';

        $return = (object) [
                    'id' => $image->getId(),
                    'path' => $base_path . $image->getImageUrl()
        ];

        return $return;
    }

    protected function upload_image(Request $request, $image_type) {
        $path = "upload/";

        switch ($image_type) {
            case "event":
                $path = "images/event/";
                break;
            case "club":
                $path = "images/club/";
                break;
            case "profile":
                $path = "images/user/";
                break;
            case "user":
                $path = "images/user/";
                break;
            case "marketing":
                $path = "images/marketingcompany/";
                break;
            case "products":
                $path = "images/products/";
                break;
            case "sport":
                $path = "images/sport/";
                break;
            default:
                $path = "images/";
                break;
        }



        $filename = $_FILES["image"]["name"];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
        $file_ext = substr($filename, strripos($filename, '.')); // get file name
        $filesize = $_FILES["image"]["size"];
        $allowed_file_types = array('.png', '.jpg', '.jpeg', '.gif');

        if (in_array($file_ext, $allowed_file_types) && ($filesize < 2000000)) {
            // Rename file
            $newfilename = md5($file_basename) . mt_rand(10000, 999999) . $file_ext;
            if (file_exists($path . $newfilename)) {
                // file already exists error
                return [
                    0, 'unexpected error'
                ];
            } else {
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $path . $newfilename)) {

                    return [
                        1, $path . $newfilename
                    ];
                } else {
                    return [
                        0, 'unexpected error'
                    ];
                }
            }
        } elseif (empty($file_basename)) {
            return [
                0, 'please select file'
            ];
        } elseif ($filesize > 2000000) {
            return [
                0, 'image is large'
            ];
        } else {
            return [
                0, 'invalid type'
            ];
        }
    }

    protected function get_club_object_by_id($clubId, $request) {

        $club = $this->getDoctrine()->getRepository('BaseBundle:WsbClub')->findOneById($clubId);
        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/sport/';
        if ($club) {
            return (object) [
                        'club_logo' => (object) [
                            'id' => $club->getProfileImg()->getId(),
                            'path' => $base_path . $club->getProfileImg()->getImageUrl(),
                        ],
                        'club_image' => (object) [
                            'id' => $club->getCoverImage()->getId(),
                            'path' => $base_path . $club->getCoverImage()->getImageUrl(),
                        ],
                        'club_name' => $club->getClubName(),
                        'rating' => (object) [
                            'total' => 5,
                            'obtain' => 5,
                        ],
            ];
        } else {
            return (object) [];
        }
    }

    protected function make_user_object_by_obejct(Entity\FosUser $user, Request $request) {

        $events = $this->getDoctrine()->getRepository('BaseBundle:WsbEvent')->findBy(['postedByUser'=>$user]);
        $userResponse = (object) [
                    'id' => $user->getId(),
                    'first_name' => $user->getFirstName(),
                    'last_name' => $user->getLastName(),
                    'user_name' => $user->getUsername(),
                    'email' => $user->getEmail(),
                    'profile_image' => $user->getProfileImage() ? $this->parse_user_image_object($user->getProfileImage(), $request) : $this->default_image($request),
                    'cover_image' => $user->getCoverImage() ? $this->parse_user_image_object($user->getCoverImage(), $request) : $this->default_image($request),
                    'location' => (object) [
                        'address' => $user->getAddress(),
                        'city' => $user->getCity(),
                        'state' => $user->getState(),
                        'country' => $user->getState(),
                        'postalCode' => $user->getPostalCode(),
                    ],
                    'others' => ($user->getOthers() == NULL) ? [] : json_decode($user->getOthers()),
                    'my_events_count' => $events ? count($events) : 0,
        ];

        return $userResponse;
    }

    protected function make_suggest_user_object_by_obejct(Entity\FosUser $user, Request $request) {
        $events = $this->getDoctrine()->getRepository('BaseBundle:WsbEvent')->findBy(['postedByUser'=>$user]);
        
        $userResponse = (object) [
                    'id' => $user->getId(),
                    'first_name' => $user->getFirstName(),
                    'last_name' => $user->getLastName(),
                    'user_name' => $user->getUsername(),
                    'email' => $user->getEmail(),
                    'profile_image' => $user->getProfileImage() ? $this->parse_user_image_object($user->getProfileImage(), $request) : $this->default_user_image($request),
                    'cover_image' => $user->getCoverImage() ? $this->parse_user_image_object($user->getCoverImage(), $request) : $this->default_image($request),
                    'location' => (object) [
                        'address' => $user->getAddress(),
                        'city' => $user->getCity(),
                        'state' => $user->getState(),
                        'country' => $user->getState(),
                        'postalCode' => $user->getPostalCode(),
                    ],
                    'others' => ($user->getOthers() == NULL) ? [] : json_decode($user->getOthers()),
                    'my_events_count' => $events ? count($events) : 0,
                 
        ];

        return $userResponse;
    }

    protected function make_user_detail_object_by_obejct(Entity\FosUser $user, Request $request) {
//$base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath().'/images/user/';
        $userResponse = [
                    'id' => $user->getId(),
                    'first_name' => $user->getFirstName(),
                    'last_name' => $user->getLastName(),
                    'user_name' => $user->getUsername(),
                    'email' => $user->getEmail(),
                    'age' => $user->getAge(),
                    'gender'=>$user->getGender() == NULL ? NULL : $user->getGender()->getGenderType(),
                    'city'=>$user->getCity(),
                    'description'=>$user->getDescription(),
                    'others' => $user->getOthers() == null ? (object) [] : json_decode($user->getOthers()),
                    'Roles' => (object) $user->getRoles(),
                    'profile_image' => ($user->getProfileImage()) ? $this->parse_user_image_object($user->getProfileImage(), $request) : $this->default_user_image($request),
                    'cover_image' => $user->getCoverImage() ? $this->parse_user_image_object($user->getCoverImage(), $request) : $this->default_image($request),
                    'location' => (object) [
                        'address' => $user->getAddress(),
                        'city' => $user->getCity(),
                        'state' => $user->getState(),
                        'country' => $user->getState(),
                        'postalCode' => $user->getPostalCode(),
                    ],
        ];

        $friendsObject1 = $this->getDoctrine()->getRepository('BaseBundle:WsbUserFriend')->findBy(['user'=>$user,'isFriend'=>1]);
        $friendsObject2 = $this->getDoctrine()->getRepository('BaseBundle:WsbUserFriend')->findBy(['friend'=>$user,'isFriend'=>1]);

        $friendsObject = array_merge($friendsObject1,$friendsObject2);

        $friends = [];
        foreach($friendsObject as $fo){
            if($fo->getFriend()->getId() == $user->getId()){
                $friends[] = $fo->getUser();
            }else if($fo->getUser()->getId() == $user->getId()){
                $friends[] = $fo->getFriend();
            }
        }

        $userResponse['buddies'] = $friends ? $this->make_user_object_by_array_of_objects($friends,$request) : [];
        $userResponse['buddies_count'] = $friends ? count($friends) : 0;



        $availability = $this->getDoctrine()->getRepository('BaseBundle:UserAvailability')->findBy(array('user'=>$user));

        $days = $this->getDoctrine()->getRepository('BaseBundle:AvailabilityDays')->findAll();
        $timings = $this->getDoctrine()->getRepository('BaseBundle:AvailabilityTimming')->findAll();

        $current_availabnility = [];
        foreach($days as $day){
            $timming = [];
            foreach($timings as $timing){

                $availability = $this->getDoctrine()->getRepository('BaseBundle:UserAvailability')->findOneBy(array('user'=>$user,'day'=>$day, 'time'=>$timing));

                if($availability){
                    if($availability->getStatus() == 1 || $availability->getStatus() == true) {
                        $timming[] = [
                            'id' => $timing->getId(),
                            'name' => $timing->getName(),
                            'status'=> 1
                        ];
                    }else{
                        $timming[] = [
                            'id' => $timing->getId(),
                            'name' => $timing->getName(),
                            'status'=> 0
                        ];
                    }
                }else{
                    $timming[] = [
                        'id' => $timing->getId(),
                        'name' => $timing->getName(),
                        'status'=> 0
                    ];
                }


            }
            $current_availabnility[] = [
                'id' => $day->getId(),
                'name' => $day->getName(),
                'timing' => $timming
            ];
        }
        $userResponse['availability'] = $current_availabnility;

        $events = $this->getDoctrine()->getRepository('BaseBundle:WsbEvent')->findByPostedByUser($user);
        $i = 0;
        $userResponse['my_events'] = [];
        foreach($events as $event){
            if($i <= 2 ){
                $userResponse['my_events'][] = $this->make_event_detail_object_by_object($event, $request);
                $i++;
            }
        }
        $userResponse['my_events_count'] = count($events);

        $events_upcomming = $this->getDoctrine()->getRepository('BaseBundle:EventPartcipant')->findBy(['user'=>$user,'status'=>1]);
        $i = 0;
        $userResponse['up_coming_events'] = [];
        foreach($events_upcomming as $event){
            if($i <= 2 ){
                    $userResponse['up_coming_events'][] = $this->make_event_detail_object_by_object($event->getEvent(), $request);
                $i++;
            }
        }
        $userResponse['up_coming_events_count'] = count($events_upcomming);
        $events_upcomming = $this->getDoctrine()->getRepository('BaseBundle:EventPartcipant')->findBy(['user'=>$user,'status'=>1]);
        $i = 0;
        $userResponse['attending_events'] = [];
        foreach($events_upcomming as $event){
            if($i <= 2 ){
                $userResponse['attending_events'][] = $this->make_event_detail_object_by_object($event->getEvent(), $request);
                $i++;
            }
        }
        $userResponse['attending_events_count'] = count($events_upcomming);
        $games = $this->getDoctrine()->getRepository('BaseBundle:WsbGames')->findBy(['sender'=>$user]);
        $i = 0;
        $userResponse['my_games'] = [];
        foreach($games as $game){
            if($i <= 2 ){
                $userResponse['my_games'][] = $this->make_game_object_by_object($game, $request);
                $i++;
            }
        }
        $userResponse['my_games_count'] = count($games);
        $games = $this->getDoctrine()->getRepository('BaseBundle:WsbGames')->findBy(['receaver'=>$user]);
        $i = 0;
        $userResponse['up_coming_games'] = [];
        foreach($games as $game){
            if($i <= 2 ){
                $userResponse['up_coming_games'][] = $this->make_game_object_by_object($game, $request);
                $i++;
            }
        }
        $userResponse['up_coming_games_count'] = count($games);
        $userResponse['is_friend'] = (int) 0;
        $cUser = $this->get_user_by_id($this->get_user_id($request));
        
        if($cUser->getId() == $user->getId()){
            //User is Same
            $userResponse['is_friend'] = (int) 2;
        }else{
            $friendsObject1 = $this->getDoctrine()->getRepository('BaseBundle:WsbUserFriend')->findOneBy(['user'=>$user,'friend'=>$cUser]);
            $friendsObject2 = $this->getDoctrine()->getRepository('BaseBundle:WsbUserFriend')->findOneBy(['friend'=>$cUser,'friend'=>$user]);
            
            if($friendsObject1 || $friendsObject2){
                if($friendsObject1){
                    if($friendsObject1->getIsFriend() == 1){
                        //We both are friends
                        $userResponse['is_friend'] = (int) 1;
                    }else{
                        //Friend Request is send but pending
                        $userResponse['is_friend'] = (int) 3;
                    }
                }
                if($friendsObject2){
                    if($friendsObject2->getIsFriend() == 1){
                        //We both are friends
                        $userResponse['is_friend'] = (int) 1;
                    }else{
                        //Request is send but pending
                        $userResponse['is_friend'] = (int) 3;
                    }
                }
            }else{
                //No Relation between both users
                $userResponse['is_friend'] = (int) 0;
            }
        }



        //Farmula

        $userPictureScore = 10;
        $nameScore = 10;
        $latlongScore = 20;
        $addressScore = 10;
        $postalCodeScore = 10;
        $aboutScore = 10;
        $sportsScore = 10;
        $genderScore = 10;
        $ageScore = 10;
        $totalScore = 0;

        if($user->getProfileImage()){
            $totalScore += $userPictureScore;
        }
        if($user->getFirstName() != ''){
            $totalScore += $nameScore;
        }
        if($user->getLatitude() != '' && $user->getLongitude() != ''){
            $totalScore += $latlongScore;
        }
        if($user->getAddress() != ''){
            $totalScore += $addressScore;
        }
        if($user->getPostalCode() != ''){
            $totalScore += $postalCodeScore;
        }
        if($user->getDescription() != ''){
            $totalScore += $aboutScore;
        }
        if($user->getOthers() != ''){
            $totalScore += $sportsScore;
        }
        if($user->getGender()){
            $totalScore += $genderScore;
        }
        if($user->getAge() != ''){
            $totalScore += $ageScore;
        }
        $userResponse['profile_score'] = $totalScore;
        
        return (object) $userResponse;
    }

    protected function make_user_object_by_array_of_objects($users, $request) {

        $userResponse = [];

        foreach ($users as $user) {

            $userResponse[] = $this->make_user_object_by_obejct($user, $request);
        }
        return $userResponse;
    }

    protected function make_sports_object_by_object(Entity\WsbSport $sport, Request $request) {

        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/sport/';

        $return = (object) array(
                    "id" => $sport->getId(),
                    "name" => $sport->getSportName(),
                    'image' => $base_path . $sport->getImageName()
        );
        return $return;
    }

    protected function make_sports_object_by_array_of_objects($sports, Request $request) {

        $return = [];

        foreach ($sports as $sport) {
            if($sport->getSportStatus() == 1){
            $return[] = $this->make_sports_object_by_object($sport, $request);
            }
        }
        return $return;
    }

    protected function make_club_object_by_object(Entity\WsbClub $club, Request $request) {
         $ClubObject = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:ClubSportsSubscribtions')->findByClub($club);
         $sports = [];
         if($ClubObject){
             foreach($ClubObject as $co){
                 $sports[] = $co->getSports();
             }
         }
          $user = $this->get_user_by_id($this->get_user_id($request));
          
         $ClubEvents = $this->getDoctrine()->getRepository('BaseBundle:WsbEvent')->findBy(['postByClub'=>$club,'postUnder'=>2]);
         
         
        $return = (object) [
                    'id' => $club->getId(),
                    'name' => $club->getClubName(),
                    'status' => $club->getClubStatus(),
                    'profile_image' => $this->parse_club_image_object($club->getProfileImg(), $request),
                    'cover_image' => $this->parse_club_image_object($club->getCoverImage(), $request),
                    'total_rating' => $club->getClubRatingTotal(),
                    'obt_rating' => $club->getClubRatingObt(),
                    'total_raters' => 15,
                    'location' => [
                        'sate' => $club->getClubState(),
                        'city' => $club->getClubCity(),
                        'country' => $club->getClubCountry(),
                        'address' => $club->getClubAddress(),
                        'postal_code' => $club->getClubPostalCode(),
                        'latitude' => $club->getClubLatitude(),
                        'longitude' => $club->getClubLongitude(),
                    ],
                    'events' => [],
                    'sports' => $ClubObject ? $this->make_sports_object_by_array_of_objects($sports,$request) : [],
                   
                    'events_count' => count($ClubEvents),
                    'distance' => '0'
        ];
        return $return;
    }

    protected function make_club_detail_object_by_object(Entity\WsbClub $club, Request $request) {

        $user = $this->get_user_by_id($this->get_user_id($request));
        
         $ClubObject = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:ClubSportsSubscribtions')->findByClub($club);
         $sports = [];
         if($ClubObject){
             foreach($ClubObject as $co){
                 $sports[] = $co->getSports();
             }
         }
         $ClubEvents = $this->getDoctrine()->getRepository('BaseBundle:WsbEvent')->findBy(['postByClub'=>$club,'postUnder'=>2]);
         $events =[];
         if($ClubEvents){
             foreach($ClubEvents as $co){
                 $events[] = $co;
             }
         }

        $is_following = 0;
        $is_favorite  = 0;

        $clubFollowing = $this->getDoctrine()->getRepository('BaseBundle:ClubFollowers')->findBy(
          [
              'club' => $club,
              'user' => $user,
              'status'=>1
          ]
        );
        $clubFavorite = $this->getDoctrine()->getRepository('BaseBundle:ClubFavorite')->findOneBy(
            [
                'club' => $club,
                'user' => $user,
                'status'=>1
            ]
        );
        if($clubFavorite){
            $is_favorite = 1;
        }
        if($clubFollowing){
            $is_following = 1;
        }
           $users = [];
         if($clubFollowing){
             foreach($clubFollowing as $co){
                 $users[] = $co->getUser();
             }
         }
         //var_dump($users);die("Called");

        $return = (object) [
                    'id' => $club->getId(),
                    'name' => $club->getClubName(),
                    'website' => $club->getClubWebsite(),
                    'phone' => $club->getClubPhone(),
                    'email' => $club->getClubEmail(),
                    'operating_hours' => $club->getClubOperatingHours(),
                    'total_rating' => $club->getClubRatingTotal(),
                    'obt_rating' => $club->getClubRatingObt(),
                    'total_raters' => 15,
                    'description' => 'sports club',
                    'location' => [
                        'sate' => $club->getClubState(),
                        'city' => $club->getClubCity(),
                        'country' => $club->getClubCountry(),
                        'address' => $club->getClubAddress(),
                        'postal_code' => $club->getClubPostalCode(),
                        'latitude' => $club->getClubLatitude(),
                        'longitude' => $club->getClubLongitude(),
                    ],
                    'admin' => $this->make_user_object_by_obejct($club->getAdmin(), $request),
                    'status' => $club->getClubStatus(),
                    'profile_image' => $this->parse_club_image_object($club->getProfileImg(), $request),
                    'cover_image' => $this->parse_club_image_object($club->getCoverImage(), $request),
                    'is_followed'=> $is_following,
                    'is_favourite' => $is_favorite,
                    'sports' => $ClubObject ? $this->make_sports_object_by_array_of_objects($sports,$request) : [],
                    'events_count' => count($ClubEvents),
                    'events' => $events ? $this->make_event_object_by_array($events,$request) : [],
                    'club_members' =>$this->make_user_object_by_array_of_objects($users,$request),
                    'refferal_link' => "referral" //$this->get('router')->generate('wsb_front_event_detail_page', array(
                                                        //    'club' => base64_encode($club->getId()),
                                                        //    'userReferral' => base64_encode($events->getPostedByUser()->getId())
                                                      //  ))
        ];

        return $return;
    }

    protected function make_club_object_by_array_of_object($clubs, $request) {

        $return = [];
        foreach ($clubs as $club) {
            $return[] = $this->make_club_object_by_object($club, $request);
        }
        return $return;
    }

    protected function make_event_object_by_object(Entity\WsbEvent $event, Request $request) {
        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/user/';
        $return = [
                    'id' => $event->getId(),
                    'name' => $event->getEventName(),
                    'start' => $event->getEventStartDate(),
                    'cover' => (object) [
                        'id' => 7,
                        'path' => $base_path . 'cover.png',
                    ],
                    'images' => [
                        (object) [
                            'id' => 7,
                            'path' => $base_path . 'cover.png',
                        ]
                    ],
                    'end' => $event->getEventEndDate(),
                    'total_players' => $event->getTotalPlayers(),
                    'going' => $event->getAttendingPlayers(),
                    'remaining' => $event->getTotalPlayers()-$event->getAttendingPlayers(),
                    'location' => [
                        'city' => $event->getCity(),
                        'state' => $event->getState(),
                        'country' => $event->getCountry(),
                        'address' => $event->getEventAddress(),
                        'latitude' => $event->getEventLat(),
                        'longitude' => $event->getEventLong(),
                    ],
                    'public_private' => $event->getEventScope(),
                    'posted_by' => $event->getPostUnder(),
                    'posted_by_club' => $event->getPostByClub() ? $this->make_club_object_by_object($event->getPostByClub(), $request) : (object) [],
                    'posted_by_user' => $event->getPostedByUser() ? $this->make_user_object_by_obejct($event->getPostedByUser(), $request) : (object) [],
        ];

        $eventParticipants = $this->getDoctrine()->getRepository('BaseBundle:EventPartcipant')->findBy(array('event'=>$event,'status'=>1));
        $participants = [];

        if($eventParticipants){
            foreach($eventParticipants as $ep){
                $participants[] = $ep->getUser();
            }

            $return['event_participants'] = !empty($participants) ? $this->make_user_object_by_array_of_objects($participants, $request): [];
                  $return['going_players']    = count($participants);
        }else{
            $return['event_participants'] = [];
                  $return['going_players']    = 0;
        }

        return (object) $return;
    }

    protected function make_game_object_by_object(Entity\WsbGames $games, Request $request) {
        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/user/';
        $return = (object) [
            'id' => $games->getId(),
            'name' => $games->getName(),
            'sport' => $games->getSports()->getSportName(),
            'message' => $games->getMessage(),
            'time'=>$games->getGameTime(),
            'isAttending' => $games->getIsAttending() ? 1 : 0,
            'notAttending' => $games->getNotAttending() ? 1 : 0,
            'maybeAttending' => $games->getMaybeAttending() ? 1 : 0,
            'location' => (object) [
                'address' => $games->getAddress(),
                'city' => $games->getCity(),
                'state' => $games->getState(),
                'country' => $games->getCountry(),
                'postalCode' => $games->getPostalCode(),
            ],
            'invitedBy'=>$this->make_user_object_by_obejct($games->getSender(),$request),
            'invitedTo'=>$this->make_user_object_by_obejct($games->getReceaver(),$request)
        ];

        return $return;
    }
    
    protected function make_game_object_by_array($games,$request){
        $response = [];
        foreach($games as $game){
            $response[] = $this->make_game_object_by_object($game, $request);
        }
        return $response;
    }

    protected function make_game_detail_object_by_object(Entity\WsbGames $game, Request $request) {
        $return = (object) [
                    "name" => $game->getName(),
                    "sports_id" => $game->getSportsId(),
                    "sender_id" => $game->getSenderId(),
                    "game_time" => $game->getGameTime(),
                    'location' => (object) [
                        'address' => $game->getAddress(),
                        'city' => $game->getCity(),
                        'state' => $game->getState(),
                        'country' => $game->getCountry(),
                        'postalCode' => $game->getPostalCode(),
                    ],
                    "created_on" => $game->getCreatedOn(),
                    "modified_on" => $game->getModifiedOn(),
                    "id" => $game->getId(),
                    "message" => $game->getMessage(),
                    'invitedBy'=>$this->make_user_object_by_obejct($game->getSender(),$request),
                    'invitedTo'=>$this->make_user_object_by_obejct($game->getReceaver(),$request)
        ];

        return $return;
    }

    protected function make_event_detail_object_by_object(Entity\WsbEvent $event, Request $request) {
        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/club/';
        if ($event->getImageId() != '') {
            $imgObj = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:EventImages')->findOneById($event->getImageId());

            if ($imgObj) {
                $cover_image = (object) [
                            'id' => $imgObj->getId(),
                            'path' => $imgObj->getImageUrl()
                ];
            } else {
                $cover_image = (object) [
                            "id" => 2,
                            "path" => $base_path . "cover.png"
                ];
            }
        } else {
            $cover_image = (object) [
                        "id" => 2,
                        "path" => $base_path . "cover.png"
            ];
        }

        $imgObj = $this->getDoctrine()->getEntityManager()->getRepository('BaseBundle:EventImages')->findByEvent($event);
        if (!empty($imgObj)) {
            foreach ($imgObj as $row) {
                $images[] = (object) [
                            'id' => $row->getId(),
                            'path' => $base_path . $row->getImageUrl()
                ];
            }
        }

        $commentsObject = $this->getDoctrine()->getRepository('BaseBundle:Comments')->findByEvent($event);
        $commentsCount = count($commentsObject);
        $commentsObjectToReturn = [];

        if ($commentsCount > 0) {
            $limit = 3;
            $count = 1;
            foreach ($commentsObject as $comment) {
                if ($count <= $limit) {
                    $commentsObjectToReturn[] = (object) [
                                'comment' => $comment->getComment(),
                                'comment_by' => $this->make_user_object_by_obejct($comment->getCommentBy(), $request),
                                'reply_of' => $comment->getReplyOf(),
                                'comment_on' => $comment->getCommentOn(),
                                'comment_on_event' => $comment->getEvent() ? $this->make_event_object_by_object($comment->getEvent(), $request) : (object) [],
                                'comment_on_club' => $comment->getClub() ? $this->make_club_object_by_object($comment->getClub(), $request) : (object) [],
                                'comment_at' => $comment->getCreatedOn()
                    ];
                    $count++;
                }
            }
        }
        $return = [
                    'id' => $event->getId(),
                    'name' => $event->getEventName(),
                    'description' => $event->getEventDescription(),
                    'start' => $event->getEventStartDate(),
                    'cover' => (object) [
                        "id" => 2,
                        "path" => $base_path . "cover.png"
                    ],
                    'images' => [
                        (object) [
                            "id" => 2,
                            "path" => $base_path . "cover.png"
                        ]
                    ],
                    'end' => $event->getEventEndDate(),
                    'location' => [
                        'city' => $event->getCity(),
                        'state' => $event->getState(),
                        'country' => $event->getCountry(),
                        'address' => $event->getEventAddress(),
                        'latitude' => $event->getEventLat(),
                        'longitude' => $event->getEventLong(),
                    ],
                    'host_name' => $event->getEventHostName(),
                    'public_private' => $event->getEventScope(),
                    'price' => $event->getEventPrice(),
                    'status' => $event->getEventStatus(),
                    'posted_by' => $event->getPostUnder(),
                    'posted_by_club' => $event->getPostByClub() ? $this->make_club_object_by_object($event->getPostByClub(), $request) : (object) [],
                    'posted_by_user' => $event->getPostedByUser() ? $this->make_user_object_by_obejct($event->getPostedByUser(), $request) : (object) [],
                    'attending_status' => $this->is_attending_event_object($event, $request),
                    'comments' => $commentsObjectToReturn,
                    'comment_count' => $commentsCount,
                    'ability_required' => $event->getEventAbility(),
                    'total_players' => $event->getTotalPlayers(),
                    'going' => $event->getAttendingPlayers(),
                    'referral_info'                  => "referral" //$this->get('router')->generate('wsb_front_event_detail_page', array(
                                                            //'eventReferral' => $event->getReferalCode(),
                                                           // 'userReferral' => base64_encode($event->getPostedByUser()->getId())
                                                       // ))
        ];

        $eventParticipants = $this->getDoctrine()->getRepository('BaseBundle:EventPartcipant')->findBy(array('event'=>$event,'status'=>1));
        $participants = [];

        if($eventParticipants){
            foreach($eventParticipants as $ep){
                $participants[] = $ep->getUser();
            }

            $return['event_participants'] = !empty($participants) ? $this->make_user_object_by_array_of_objects($participants, $request): [];
            $return['going_players']    = count($participants);

        }else{

            $return['event_participants'] = [];
            $return['going_players']    = 0;

        }

        return (object) $return;
    }

    protected function make_event_object_by_array($events, Request $request) {

        $return = [];

        foreach ($events as $event) {
            $return[] = $this->make_event_object_by_object($event, $request);
        }

        return $return;
    }

    protected function get_loyality_card_object_by_object(Entity\WsbLoyaltyCard $card, Request $request) {
        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/marketingcompany/';
        $noimg = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/no-image.png';
        $return = (object) [
                    'company_name' => $card->getMarketingCompany() == NULL ? NULL : $card->getMarketingCompany()->getCompanyName(),
                    'id' => $card->getId(),
                    'name' => $card->getLoyaltyCardName(),
                    'amount' => $card->getLoyaltyCardDiscountAmount(),
                    'promocode' => $card->getLoyaltyCardCode(),
                    'discount_type' => 'percentage',
                    'desc' => $card->getLoyaltyCardDescription(),
                    'status' => $card->getLoyaltyCardStatus(),
                    'start_date' => $card->getLoyaltyCardOfferStartDate() == NULL ? (object)[] : $card->getLoyaltyCardOfferStartDate(),
                    'end_date' => $card->getLoyaltyCardOfferEndDate() == NULL ? (object)[] : $card->getLoyaltyCardOfferEndDate(),
                    'created_on' => $card->getLoyaltyCardCreatedDate(),
                    'location' => [
                        'city' => $card->getCity(),
                        'state' => $card->getState(),
                        'country' => $card->getCountry(),
                        'address' => $card->getAddress(),
                        'latitude' => $card->getLatitude(),
                        'longitude' => $card->getLongitude(),
                    ],
                    'Image' => (($card->getMarketingCompany() == NULL ? NULL : $card->getMarketingCompany()->getImageName()) ? $card->getMarketingCompany() == NULL ? NULL : $base_path . $card->getMarketingCompany()->getImageName() : $noimg )
        ];
        
        return $return;
    }

    protected function get_loyality_cardList_object_by_object(Entity\WsbLoyaltyCard $loyal, Request $request) {

        $base_path = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/marketingcompany/';
        $noimg = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/images/no-image.png';
        $return = (object) [
                    'company_name' => $loyal->getMarketingCompany() == NULL ? NULL : $loyal->getMarketingCompany()->getCompanyName(),
                    'id' => $loyal->getId(),
                    'name' => $loyal->getLoyaltyCardName(),
                    'amount' => $loyal->getLoyaltyCardDiscountAmount(),
                    'expiry_date' => $loyal->getLoyaltyCardOfferEndDate() == NULL ? (object)[] : $loyal->getLoyaltyCardOfferEndDate(),
                    'discount_type' => 'percentage',
                    'location' => [
                        'city' => $loyal->getCity(),
                        'state' => $loyal->getState(),
                        'country' => $loyal->getCountry(),
                        'address' => $loyal->getAddress(),
                        'latitude' => $loyal->getLatitude(),
                        'longitude' => $loyal->getLongitude(),
                    ],
                    'Image' => (($loyal->getMarketingCompany() == NULL ? NULL : $loyal->getMarketingCompany()->getImageName()) ? $loyal->getMarketingCompany() == NULL ? NULL : $base_path . $loyal->getMarketingCompany()->getImageName() : $noimg ),
        ];
        return $return;
    }

    protected function make_loyalty_list_object_by_object($cards, Request $request) {
        $return = [];
        foreach ($cards as $card) {
            $return[] = $this->get_loyality_cardList_object_by_object($card, $request);
        }

        return $return;
    }

    protected function get_days_timings(){

        $days = $this->getDoctrine()->getRepository('BaseBundle:AvailabilityDays')->findAll();
        $timings = $this->getDoctrine()->getRepository('BaseBundle:AvailabilityTimming')->findAll();

        $return = [];

        foreach($days as $day){
            $return['days'][] = (object) [
                'id'=>$day->getId(),
                'name'=>$day->getName()
            ];
        }
        foreach($timings as $timing){
            $return['timings'][] = (object) [
                'id'=>$timing->getId(),
                'name'=>$timing->getName()
            ];
        }

        return $return;
    }

    protected function make_coach_object_by_object(Entity\FosUser $user, $request){


        $return = [
            'coach_type'=>1,
            'id' => $user->getId(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'user_name' => $user->getUsername(),
            'email' => $user->getEmail(),
            'profile_image' => $user->getProfileImage() ? $this->parse_user_image_object($user->getProfileImage(), $request) : $this->default_image($request),
            'cover_image' => $user->getCoverImage() ? $this->parse_user_image_object($user->getCoverImage(), $request) : $this->default_image($request),
            'location' => (object) [
                'address' => $user->getAddress(),
                'city' => $user->getCity(),
                'state' => $user->getState(),
                'country' => $user->getState(),
                'postalCode' => $user->getPostalCode(),
            ]
        ];
        $sports = [];
        $userSports = $this->getDoctrine()->getRepository('BaseBundle:UserSports')->findBy(['user'=>$user,'canCoch'=>1]);
        if($userSports){
            foreach ($userSports as $uS){
                $sports[] = [
                    'level' => $uS->getLevel(),
                    'sports' => $this->make_sports_object_by_object($uS->getSports(),$request),
                    'club' => $uS->getCochAtClub() ? $this->make_club_object_by_object($uS->getCochAtClub(), $request) : (object) []
                ];
            }
        }
        $return['sports'] = $sports;
        return (object) $return;
    }

    protected function make_coach_detail_object_by_object(Entity\Instructors $coach, $request){

        $clubInstructor = $this->getDoctrine()->getRepository('BaseBundle:ClubInstructors')->findOneBy(['instructor'=>$coach]);

        $return = [
           
            'name' => $coach->getName(),
            'image' => $coach->getImage(),
            'facebook' => $coach->getFacebook(),
            'google'    => $coach->getGooglePlus(),
            'created_on' => $coach->getCreatedOn(),
            'club' => $clubInstructor != NULL ? $this->make_club_detail_object_by_object($clubInstructor->getClub(), $request) : (object) [],
            'user' => $coach->getUser() ? $this->make_user_detail_object_by_obejct($coach->getUser(), $request) : (object) []
        ];

        return (object) $return;
    }

    protected function make_coach_object_by_array_of_objects($coaches, $request){

        $return = [];

        foreach($coaches as $coach){
            $return[] = $this->make_coach_object_by_object($coach, $request);
        }

        return $return;
    }

    protected function get_restricted_users($newArray = []){
        $GlobalArray = [14];
        $array = array_merge($newArray,$GlobalArray);
        return $array;
    }

    protected $_SESSION_KEY_INDEX = 'session_key';
    protected $_SESSION_KEY = '';
    protected $_RESPONSE_STATUS_INDEX = 'response_status';
    protected $_RESPONSE_STATUS_SUCCESS = 1;
    protected $_RESPONSE_STATUS_FAILED = 0;
    protected $_RESPONSE_MSG_INDEX = 'msg';
    protected $_RESPONSE_MSG_DESC_INDEX = 'msg_desc';
    protected $_USER_DATA_INDEX = 'user_data';
    protected $_USERS_ARRAY_INDEX = 'users_list';
    protected $_FRIEND_DATA_INDEX = 'friend_data';
    protected $_SPORTS_DATA_INDEX = 'sports_data';
    protected $_FRIENDS_LIST_DATA_INDEX = 'friends_list';
    protected $_OTHER_USER_DATA_LIST_INDEX = 'other_user';
    protected $_EVENTS_LIST_INDEX = 'events';
    protected $_GAMES_LIST_INDEX = 'games';
    protected $_EVENT_DETAIL_INDEX = 'event_info';
    protected $_CLUBS_LIST_INDEX = 'clubs';
    protected $_CLUBS_LIST_FOLLOW_INDEX = 'follow club';
    protected $_CLUB_DETAIL_INDEX = 'club_info';
    protected $_IMAGE_INFO_INDEX = 'image_info';
    protected $_PROMO_CODES_TICKETS = 'promo_codes';
    protected $_PROMO_CODE_TICKET = 'promo_code';
    protected $_EVENT_INVITATIONS_INDEX = 'events';
    protected $_LOYALTY_LIST_INDEX = 'cards';
    protected $_LOYALTY_CARDLIST_INDEX = 'loyalties';
    protected $_EXTRA_INFO_INDEX = 'extra';
    protected $_COACHES_LIST_INDEX = 'coaches';
    protected $_COMMENTS_LIST_INDEX = 'comments';
    protected $_MESSAGES_LIST_INDEX = 'messages';
    protected $_THREAD_LIST_INDEX = 'threads';
    
    protected $_ALL_LOGIN_INDEX = 'access denied';
    protected $_LOGIN_DESCRIPTION_INDEX = ["please login first."];
    protected $_EVENT_CREATE_MSG_INDEX = 'no events found';
    protected $_EVENT_CREATE_DESC_INDEX = 'no events found';
    protected $_EVENT_CREATE_SUCCESS_MSG_INDEX = 'events list';
    protected $_EVENT_CREATE_SUCCESS_MSG_DESC_INDEX = ["events list found."];
    protected $_NOT_ATTENDING_EVENT_MSG_INDEX = 'you Are not attending this event';
    protected $_NOT_ATTENDING_EVENT_MSG_VAL = 'event_id is not valid';
    protected $_NOT_ATTENDING_EVENT_DESC_INDEX = ["you are not Attending this event."];
    protected $_NOT_ATTENDING_EVENT_DESC_VAL = ["please enter valid event id."];
    protected $_NOT_ATTENDING_SUCCESS_MSG_INDEX = 'you are not Attending this event';
    protected $_NOT_ATTENDING_SUCCESS_MSG_DESC_INDEX = ["you are not attending this event."];
    protected $_IS_ATTENDING_EVENT_MSG_INDEX = 'missing required data';
    protected $_IS_ATTENDING_EVENT_DESC_INDEX = ["request_id is missing."];
    protected $_IS_ATTENDING_SUCCESS_MSG_INDEX = 'you are attending this event';
    protected $_IS_ATTENDING_SUCCESS_MSG_DESC_INDEX = ["you are attending this event."];
    protected $_IS_ATTENDING_EVENT_BY_ID_MSG_INDEX = 'missing required data';
    protected $_IS_ATTENDING_EVENT_BY_ID_DESC_INDEX = ["event_id is invalid."];
    protected $_IS_ATTENDING_SUCCESS_BY_ID_MSG_INDEX = 'you are attending this event';
    protected $_IS_ATTENDING_SUCCESS_BY_ID_MSG_DESC_INDEX = ["you are attending this event."];
    protected $_REJECT_EVENT_MSG_INDEX = 'missing required data';
    protected $_REJECT_EVENT_MSG_DESC_INDEX = ["request_id is missing."];
    protected $_REJECT_EVENT_RESPONSE_INDEX = 'already rejected';
    protected $_REJECT_EVENT_RESPONSE_DESC_INDEX = ["request you are trying to delete is either not existed or already denied."];
    protected $_REJECT_EVENT_RESPONSE_MSG_INDEX = 'request denied';
    protected $_REJECT_EVENT_RESPONSE_MSG_DESC_INDEX = ["event invitation request denied."];
    protected $_ACCEPT_EVENT_MSG_INDEX = 'Invitation Id not exist';
    protected $_ACCEPT_EVENT_MSG_DESC_INDEX = ["event invitation request denied."];
    protected $_ACCEPT_EVENT_RESPONSE_DESC_INDEX = 'No Ivitation Exist';
    protected $_ACCEPT_EVENT_RESPONSE_INDEX = ["Invitaion Not Accepted."];
    protected $_ACCEPT_EVENT_SUCCESS_INDEX = 'Invitation Accepted Successfully';
    protected $_ACCEPT_EVENT_SUCCESS_RESPONSE_INDEX = ["Invitaion accept."];
    protected $_GET_INVITATION_RESPONSE_INDEX = 'no pending event invitations';
    protected $_GET_INVITATION_RESPONSE_DESC_INDEX = ["there are no event invitations for you."];
    protected $_GET_INVITATION_MSG_DESC_INDEX = 'events found';
    protected $_GET_INVITATION_MSG_INDEX = ["events available."];
    protected $_SEND_EVENT_INVITATION_MSG_INDEX = 'event_id, user_id is missing';
    protected $_SEND_EVENT_INVITATION_DESC_INDEX = ["event_id, user_id is missing."];
    protected $_SEND_EVENT_INVITATION_RESPONSE_INDEX = 'invalid event_id';
    protected $_SEND_EVENT_INVITATION_RESPONSE_DESC_INDEX = ["invalid event"];
    protected $_SEND_EVENT_INVITATION_SUCCESS_INDEX = 'invitation send';
    protected $_SEND_EVENT_INVITATION_SUCCESS_DESC_INDEX = ["event invitation send."];
    protected $_GET_EVENT_DETAIL_MSG_INDEX = 'missing required data';
    protected $_GET_EVENT_DETAIL_DESC_INDEX = ["event_id is missing."];
    protected $_GET_EVENT_DETAIL_RESPONSE_INDEX = 'invalid event id';
    protected $_GET_EVENT_DETAIL_RESPONSE_DESC_INDEX =["event_id is invalid."];
    protected $_GET_EVENT_DETAIL_SUCCESS_MSG_INDEX ='event found';
    protected $_GET_EVENT_DETAIL_SUCCESS_DESC_INDEX =["event_id is valid."];
    protected $_GET_EVENT_RESPONSE_INDEX = 'no events found';
    protected $_GET_EVENT_RESPONSE_DESC_INDEX =["no event found"];
    protected $_GET_EVENT_SUCCESS_MSG_INDEX ='events list';
    protected $_GET_EVENT_SUCCESS_DESC_INDEX =["events list found."];
    protected $_GET_EVENT_FAIL_INDEX ='missing required data';
    protected $_GET_EVENT_FAIL_DESC_INDEX =["latitude and longitude are required in case of by_location."];
    
    protected $_GET_PROMO_LIST_MSG_INDEX = 'no promo codes available right now';
    protected $_GET_PROMO_LIST_DESC_INDEX  =["no promo codes available right now."];
    protected $_GET_PROMO_LIST_SUCCESS_MSG_INDEX ='promo codes';
    protected $_GET_PROMO_LIST_SUCCESS_DESC_INDEX =["loyalty cards list found."];
    protected $_GET_PROMO_DETAIL_MSG_INDEX =  'required parameter id is missing';
    protected $_GET_PROMO_DETAIL_DESC_INDEX  =["required parameter id is missing."];
    protected $_GET_PROMO_DETAIL_SUCCESS_MSG_INDEX ='no promo code available right now';
    protected $_GET_PROMO_DETAIL_SUCCESS_DESC_INDEX =["no promo code available right now."];
    protected $_GET_PROMO_DETAIL_SUCCESS_MESSAGE_INDEX ='promo codes';
    protected $_GET_PROMO_DETAIL_SUCCESS_DESCRIPTION_INDEX =["loyalty cards list found."];
    
    protected $_GET_ALL_SPORTS_MSG_INDEX =  'no sports found';
    protected $_GET_ALL_SPORTS_DESC_INDEX  =["no sports found in system"];
    protected $_GET_ALL_SPORTS_SUCCESS_DESC_INDEX ='sports found';
    protected $_GET_ALL_SPORTS_SUCCESS_MESSAGE_INDEX =["sports found in system"];
    
    protected $_POST_COMMENT_MSG_INDEX =  'required parameters are missing';
    protected $_POST_COMMENT_DESC_INDEX  =["event_id or club_id is required."];
    protected $_POST_COMMENT_SUCCESS_MSG_INDEX ='comment posted';
    protected $_POST_COMMENT_SUCCESS_DESC_INDEX = ["comment has been posted."];
    protected $_POST_COMMENT_MESSAGE_INDEX =  'required parameters are missing';
    protected $_POST_COMMENT_DESCRIPTION_INDEX  =["comment text cannot be null."];
    protected $_GET_COMMENT_MESSAGE_INDEX  ='comments not exists in this event';
    protected $_GET_COMMENT_DESCRIPTION_INDEX  =["comments not exists."];
    protected $_GET_COMMENT_SUCCESS_MSG_INDEX  ='comments listed';
    protected $_GET_COMMENT_SUCCESS_DESC_INDEX  =["All comments listed."];
    
    protected $_GET_THREAD_BY_USER_MSG_INDEX =  'thread info not found';
    protected $_GET_THREAD_BY_USER_DESC_INDEX  =["not found"];
    protected $_GET_THREAD_BY_USER_SUCCESS_MSG_INDEX = 'messages found';
    protected $_GET_THREAD_BY_USER_SUCCESS_DESC_INDEX = ["messages found"];
    protected $_GET_MESSAGE_MSG_INDEX =  'no message found';
    protected $_GET_MESSAGE_DESC_INDEX  =["no message found"];
    protected $_GET_MESSAGE_SUCCESS_MSG_INDEX = 'messages found';
    protected $_GET_MESSAGE_SUCCESS_DESC_INDEX = ["messages found"];
    protected $_CREATE_MESSAGE_SUCCESS_MSG_INDEX = 'Message created successfully';
    protected $_CREATE_MESSAGE_SUCCESS_DESC_INDEX = ["message created"];
    protected $_CREATE_THREAD_SUCCESS_MSG_INDEX = 'Thread created successfully';
    protected $_CREATE_THREAD_SUCCESS_DESC_INDEX = ["thread created"];
    protected $_CREATE_THREAD_MSG_INDEX =  'Thread not created';
    protected $_CREATE_THREAD_DESC_INDEX  =["thread creation failed"];
    protected $_CREATE_THREAD_SUCCESS_MESSAGE_INDEX = 'Thread created successfully';
    protected $_CREATE_THREAD_SUCCESS_DESCRIPTION_INDEX = ["thread created"];
    protected $_CREATE_THREAD_RESPONSE_MSG_INDEX =  'Undefined required data';
    protected $_CREATE_THREAD_RESPONSE_DESC_INDEX  =[
        "in case of user used_id, message and subject is required",
        "in case of event event_id, message and subject is required",
        "in case of club club_id, message and subject is required",
        ];
    
    protected $_REGISTER_RESPONSE_MSG_INDEX =  'Email or username already in use';
    protected $_REGISTER_RESPONSE_MSG_REQUIRED_FEILDS =  'Required information is missing';
    protected $_REGISTER_RESPONSE_DESC_INDEX  =["Email or username already in use"];
    protected $_REGISTER_SUCCESS_MESSAGE_INDEX =  'user created';
    protected $_REGISTER_SUCCESS_DESCRIPTION_INDEX = ["user successfully created"];
    protected $_EDIT_PROFILE_PICTURE_MSG_INDEX = 'failed to upload user image';
    protected $_EDIT_PROFILE_PICTURE_DESC_INDEX  = ["failed to upload user image."];
    protected $_EDIT_PROFILE_PICTURE_SUCCESS_MESSAGE_INDEX =  'user profile image updated';
    protected $_EDIT_PROFILE_PICTURE_SUCCESS_DESCRIPTION_INDEX = ["user profile image updated."];
    protected $_EDIT_PROFILE_MSG_INDEX = 'failed to update';
    protected $_EDIT_PROFILE_DESC_INDEX  = ["failed to update."];
    protected $_EDIT_PROFILE_SUCCESS_MESSAGE_INDEX =  'user profile updated';
    protected $_EDIT_PROFILE_SUCCESS_DESCRIPTION_INDEX = ["user profile updated."];
    protected $_UPDATE_PASSWORD_SUCCESS_MESSAGE_INDEX =  'Password updated';
    protected $_UPDATE_PASSWORD_SUCCESS_DESCRIPTION_INDEX =  ["Password updated successfully"];
    protected $_UPDATE_PASSWORD_MSG_INDEX = 'Please enter correct Password';
    protected $_UPDATE_PASSWORD_DESC_INDEX  =  ["invalid existing Password"];
    protected $_UPDATE_PASSWORD_MESSAGE_INDEX = 'Please enter Password';
    protected $_UPDATE_PASSWORD_DESCRIPTION_INDEX  = ["Password Field is Empty"];
    protected $_UPDATE_PASSWORD_MESSAGE_ID_INDEX = 'Enter User Id does not exists';
    protected $_UPDATE_PASSWORD_DESCRIPTION_ID_INDEX  = ["invalid User Id"];
    protected $_GET_USER_PROFILE_SUCCESS_MESSAGE_INDEX =   'both profiles';
    protected $_GET_USER_PROFILE_SUCCESS_DESCRIPTION_INDEX = ["both profiles are available."];
    protected $_GET_USER_PROFILE_MSG_INDEX = 'something is not right';
    protected $_GET_USER_PROFILE_DESC_INDEX  =  ["somethings is not right both user and buddy id is invalid."];
    protected $_GET_FACEBOOK_PROFILE_SUCCESS_MESSAGE_INDEX =   'User Logged In';
    protected $_GET_FACEBOOK_PROFILE_SUCCESS_DESCRIPTION_INDEX =  ["user is logged in"];
    protected $_GET_FACEBOOK_PROFILE_MSG_INDEX = 'Enter Email does not exists';
    protected $_GET_FACEBOOK_PROFILE_DESC_INDEX  =  ["invalid Email Id"];
    protected $_GEN_ACCESS_TOKEN_SUCCESS_MESSAGE_INDEX =   'users found';
    protected $_GEN_ACCESS_TOKEN_SUCCESS_DESCRIPTION_INDEX =  ["users found."];
    protected $_GEN_ACCESS_TOKEN_MSG_INDEX =  'users not found';
    protected $_GEN_ACCESS_TOKEN_DESC_INDEX  =  ["user not found."];
    
    protected $_SEND_FRIEND_REQUEST_SUCCESS_MESSAGE_INDEX =   'friend request send';
    protected $_SEND_FRIEND_REQUEST_SUCCESS_DESCRIPTION_INDEX =   ["friend request send"];
    protected $_SEND_FRIEND_REQUEST_SUCCESS_MSG_INDEX =   'already send';
    protected $_SEND_FRIEND_REQUEST_SUCCESS_DESC_INDEX =   ["Friend Request Already sent"];
    protected $_SEND_FRIEND_REQUEST_MSG_INDEX =  'invalid user';
    protected $_SEND_FRIEND_REQUEST_DESC_INDEX  =   ["Friend id is invalid"];
    protected $_GET_FRIEND_REQUEST_SUCCESS_MSG_INDEX =   'friends found';
    protected $_GET_FRIEND_REQUEST_SUCCESS_DESC_INDEX = ["friends found in system"];
    protected $_GET_FRIEND_REQUEST_MSG_INDEX = 'friends not found';
    protected $_GET_FRIEND_REQUEST_DESC_INDEX  =  ["friends not found in system #SYSTEM ERROR#"];
    protected $_GET_FRIEND_REQUEST_DESCRIPTION_INDEX  =   ["friends not found in system"];
    protected $_REJECT_FRIEND_REQUEST_FAIL_MSG_INDEX =   'request already declined';
    protected $_REJECT_FRIEND_REQUEST_FAIL_DESC_INDEX = ["Request already declined."];
    protected $_REJECT_FRIEND_REQUEST_MSG_INDEX = 'request declined';
    protected $_REJECT_FRIEND_REQUEST_DESC_INDEX  = ["User request rejected."];
    protected $_REJECT_FRIEND_REQUEST_RESPONSE_MSG_INDEX = 'disowned request';
    protected $_REJECT_FRIEND_REQUEST_RESPONSE_DESC_INDEX  = ["you are not the one who received this request."];
    protected $_REJECT_FRIEND_REQUEST_RESPONSE_DATA_MSG_INDEX = 'required data is missing';
    protected $_REJECT_FRIEND_REQUEST_RESPONSE_DATA_DESC_INDEX  = ["req_id is missing"];
    protected $_ACCEPT_FRIEND_REQUEST_FAIL_MSG_INDEX =   'request already declined';
    protected $_ACCEPT_FRIEND_REQUEST_FAIL_DESC_INDEX = ["Request already declined."];
    protected $_ACCEPT_FRIEND_REQUEST_SUCCESS_MSG_INDEX =   'request accepted';
    protected $_ACCEPT_FRIEND_REQUEST_SUCCESS_DESC_INDEX = ["Friend request accepted."];
    protected $_ACCEPT_FRIEND_REQUEST_FAILED_MSG_INDEX =    'disowned request';
    protected $_ACCEPT_FRIEND_REQUEST_FAILED_DESC_INDEX = ["you are not the one who received this request."];
    protected $_ACCEPT_FRIEND_REQUEST_RESPONSE_MSG_INDEX =    'required data is missing';
    protected $_ACCEPT_FRIEND_REQUEST_RESPONSE_DESC_INDEX = ["req_id is missing"];
    protected $_GET_SENT_FRIEND_REQUEST_SUCCESS_MSG_INDEX =    'requests listed';
    protected $_GET_SENT_FRIEND_REQUEST_SUCCESS_DESC_INDEX = ["Friend request listed."];
    protected $_GET_SENT_FRIEND_REQUEST_FAIL_MSG_INDEX =    'no requests';
    protected $_GET_SENT_FRIEND_REQUEST_FAIL_DESC_INDEX = ["you dont have send any friend request"];
    protected $_GET_SENT_FRIEND_REQUEST_RESPONSE_MSG_INDEX =    'no requests';
    protected $_GET_SENT_FRIEND_REQUEST_RESPONSE_DESC_INDEX = ["you dont have send any friend request"];
    protected $_GET_ALL_FRIEND_SUCCESS_MSG_INDEX =    'requests listed';
    protected $_GET_ALL_FRIEND_SUCCESS_DESC_INDEX = ["Friend request listed."];
    protected $_GET_ALL_FRIEND_FAIL_MSG_INDEX =   'No Friends';
    protected $_GET_ALL_FRIEND_FAIL_DESC_INDEX = ["you do not have any friend"];
    protected $_GET_ALL_FRIEND_RESPONSE_MSG_INDEX =    'no requests';
    protected $_GET_ALL_FRIEND_RESPONSE_DESC_INDEX = ["missing require data"];
    protected $_BLOCK_FRIEND_SUCCESS_MSG_INDEX =     'user blocked';
    protected $_BLOCK_FRIEND_SUCCESS_DESC_INDEX = ["user blocked."];
    protected $_BLOCK_FRIEND_FAIL_MSG_INDEX =   'invalid block_user_id';
    protected $_BLOCK_FRIEND_FAIL_DESC_INDEX = ["blocked_user_id is invalid."];
    protected $_BLOCK_FRIEND_RESPONSE_MSG_INDEX =  'missing required data';
    protected $_BLOCK_FRIEND_RESPONSE_DESC_INDEX = ["blocked_user_id or user session is missing."];
    
    protected $_GET_CLUBS_SUCCESS_MSG_INDEX =    'events list';
    protected $_GET_CLUBS_SUCCESS_DESC_INDEX =  ["events list found."];
    protected $_GET_CLUBS_FAIL_MSG_INDEX =   'missing required data';
    protected $_GET_CLUBS_FAIL_DESC_INDEX = ["latitude and longitude are required in case of location."];
    protected $_GET_CLUBS_RESPONSE_MSG_INDEX = 'no clubs found';
    protected $_GET_CLUBS_RESPONSE_DESC_INDEX = ["no clubs found."];
    protected $_GET_CLUBS_DETAIL_SUCCESS_MSG_INDEX =    'club found';
    protected $_GET_CLUBS_DETAIL_SUCCESS_DESC_INDEX = ["club_id is valid."];
    protected $_GET_CLUBS_DETAIL_FAIL_MSG_INDEX =   'invalid event id';
    protected $_GET_CLUBS_DETAIL_FAIL_DESC_INDEX =  ["club_id is invalid."];
    protected $_GET_CLUBS_DETAIL_RESPONSE_MSG_INDEX = 'missing required data';
    protected $_GET_CLUBS_DETAIL_RESPONSE_DESC_INDEX =["club_id is missing."];
    protected $_UPLOAD_CLUBS_SUCCESS_MSG_INDEX =   'image uploaded';
    protected $_UPLOAD_CLUBS_SUCCESS_DESC_INDEX = 'image is uploaded';
     protected $_FOLLOW_ACTION_SUCCESS_MSG_INDEX =   'club followed';
    protected $_FOLLOW_ACTION_SUCCESS_DESC_INDEX = 'you have successfully followed the club';
    protected $_FOLLOW_ACTION_FAIL_MSG_INDEX =   'you have already following this club';
    protected $_FOLLOW_ACTION_FAIL_DESC_INDEX =  ['you have already following this club'];
    protected $_FOLLOW_ACTION_RESPONSE_MSG_INDEX = 'club_id is invalid';
    protected $_FOLLOW_ACTION_RESPONSE_DESC_INDEX = ['some required parameters are invalid'];
    protected $_FOLLOW_ACTION_RESPONSE_MESSAGE_INDEX = 'club_id is missing';
    protected $_FOLLOW_ACTION_RESPONSE_DESCRIPTION_INDEX = ['some required parameters are missing'];
    protected $_FOLLOW_ACTION_RESPONSE_MSG_SEC_INDEX = 'club_id is invalid';
    protected $_FOLLOW_ACTION_RESPONSE_DESC_SEC_INDEX = ['some required parameters are invalid'];
    protected $_FOLLOW_ACTION_SUCCESS_MESSAGE_INDEX =   'club followed';
    protected $_FOLLOW_ACTION_SUCCESS_DESCRIPTION_INDEX = 'you have successfully followed the club';
    protected $_UNFOLLOW_ACTION_SUCCESS_MSG_INDEX =   'club un followed';
    protected $_UNFOLLOW_ACTION_SUCCESS_DESC_INDEX = 'you have successfully un followed the club';
    protected $_UNFOLLOW_ACTION_FAIL_MSG_INDEX =   'you have already un following this club';
    protected $_UNFOLLOW_ACTION_FAIL_DESC_INDEX =  ['you have already un following this club'];
    protected $_UNFOLLOW_ACTION_RESPONSE_MSG_INDEX = 'you have already un following this club';
    protected $_UNFOLLOW_ACTION_RESPONSE_DESC_INDEX = ['you have already un following this club'];
    protected $_UNFOLLOW_ACTION_RESPONSE_MESSAGE_INDEX = 'club_id is invalid';
    protected $_UNFOLLOW_ACTION_RESPONSE_DESCRIPTION_INDEX =  ['some required parameters are invalid'];
    protected $_UNFOLLOW_ACTION_RESPONSE_MSG_SEC_INDEX = 'club_id is missing';
    protected $_UNFOLLOW_ACTION_RESPONSE_DESC_SEC_INDEX = ['some required parameters are missing'];
     protected $_FAVORITE_ACTION_SUCCESS_MSG_INDEX =   'club un followed';
    protected $_FAVORITE_ACTION_SUCCESS_DESC_INDEX = 'you have successfully un followed the club';
    protected $_FAVORITE_ACTION_FAIL_MSG_INDEX =   'you have already un following this club';
    protected $_FAVORITE_ACTION_FAIL_DESC_INDEX =  ['you have already un following this club'];
    protected $_FAVORITE_ACTION_RESPONSE_MSG_INDEX = 'you have already un following this club';
    protected $_FAVORITE_ACTION_RESPONSE_DESC_INDEX = ['you have already un following this club'];
    protected $_FAVORITE_ACTION_RESPONSE_MESSAGE_INDEX = 'club_id is invalid';
    protected $_FAVORITE_ACTION_RESPONSE_DESCRIPTION_INDEX =  ['some required parameters are invalid'];
    protected $_GAME_DETAIL_INDEX = 'game list found';
    protected $_GET_COACH_INFO_RESPONSE_MSG_SEC_INDEX = 'missing required data';
    protected $_GET_COACH_INFO_RESPONSE_DESC_SEC_INDEX = ['some required parameters are missing'];
  
   

}
