<?php

namespace BaseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;

use BaseBundle\Entity\User;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        $userAdmin = new User();

        $userAdmin->setUsername('System');
        $userAdmin->setEmail('system@example.com');            
        $userAdmin->setPlainPassword('test');
        $userAdmin->setRoles(array('ROLE_SUPER_ADMIN'));

        $manager->persist($userAdmin);
        $manager->flush();
    }
}

