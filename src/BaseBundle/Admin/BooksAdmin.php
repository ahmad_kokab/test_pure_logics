<?php

namespace BaseBundle\Admin;
 
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
 
class BooksAdmin extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
               
                ->add('title', 'text', array('label' => 'Name','required'=>true))
                ->add('author', 'sonata_type_model', array('label' => 'Author','required'=>true))
                ->add('publishedYear', 'sonata_type_datetime_picker', array('label' => 'Published Year','required'=>true))
                ->add('rack', 'sonata_type_model', array('label' => 'Rack','required'=>true))
                ->end()
        ;
    }

    public function prePersist($object) {
        $books = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getRepository('BaseBundle\Entity\Books')->findBy(['rack'=>$object->getRack()]);
        if(count($books) <= 10) {
            $object->setCreatedOn(new \DateTime("now"));
            parent::prePersist($object);
        }else{
            $this->getRequest()->getSession()->getFlashBag()->add("error", "You can only add 10 books per Rack.");
        }
    }

    public function preUpdate($object) {
        $object->setModifiedOn(new \DateTime("now"));
        $books = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getRepository('BaseBundle\Entity\Books')->findBy(['rack'=>$object->getRack()]);
        $booksInThisRack = [];
        foreach ($books as $book){
            $booksInThisRack[] = $book->getId();
        }
        if(in_array($object->getId(),$booksInThisRack)) {
            parent::preUpdate($object);
        }else{
            if(count($books) <= 10) {
                $object->setModifiedOn(new \DateTime("now"));
                parent::prePersist($object);
            }else{
                $object->setCreatedOn(NULL);
                $this->getRequest()->getSession()->getFlashBag()->add("error", "You can only add 10 books per Rack.");
            }
        }
    }
 
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
       
         $datagridMapper
                ->add('title')
                ->add('author')
                ->add('publishedYear')
                ->add('rack')
        ;
    }
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
          $listMapper
                ->addIdentifier('title')
                ->addIdentifier('author')
                ->addIdentifier('rack')
                ->add('_action', 'actions', ['actions' => ['edit' => [], 'delete' => []]]);
    }
}

