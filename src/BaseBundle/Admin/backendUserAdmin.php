<?php

namespace BaseBundle\Admin;
 
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use BaseBundle\Entity\DeletedLogs;
 
class BackendUserAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('username', 'text', array('label' => 'User Name'))
                ->add('email', 'text', array('label' => 'Email'))
                ->add('password', 'text', array('label' => 'Password'))
                
                ->add("is_super_admin",null, array('required' => false, "label" => "Is Super Admin"))

        ;
    }
 
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
       $datagridMapper
                ->add('username')
                ->add('email')
                ->add('password')
                ->add('is_super_admin')

        ;
    }
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
         $listMapper
                ->addIdentifier('username')
                ->addIdentifier('email')
                ->addIdentifier('password')
                ->addIdentifier('is_super_amdin','boolean')
                ->add('_action', 'actions', ['actions' => ['edit' => [], 'delete' => []]]);
            
    }
    public function preRemove($object){
         
        $toBeDeleted['backendUser'] = $object;
        
        $deletedInfo = new DeletedLogs();
        
        $deletedInfo->setType('backendUser');
        $deletedInfo->setServerObject(json_encode($_SERVER));
        $deletedInfo->setRequestObject(json_encode($_REQUEST));
        $deletedInfo->setDeletedInfo(serialize($toBeDeleted));
        $deletedInfo->setCreatedOn(new \DateTime("now"));
        
        $ins = $this->getConfigurationPool()->getContainer();
        
        $em = $this->getModelManager()->getEntityManager('BaseBundle\Entity\DeletedLogs');
        $em->persist($deletedInfo);
        $em->flush();
        
   }
    
    
}

