<?php

namespace BaseBundle\Admin;
 
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
 
class AuthorAdmin extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
               
                ->add('name', 'text', array('label' => 'Name','required'=>true))
                ->add('bornAt', 'sonata_type_datetime_picker', array('label' => 'Born At','required'=>true))
                ->add('diedAt', 'sonata_type_datetime_picker', array('label' => 'Died At'))
                ->end()
        ;
    }
       

 
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
       
         $datagridMapper
               ->add('name')
               ->add('bornAt')
               ->add('diedAt')

        ;
    }
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
          $listMapper
                ->addIdentifier('name')
                ->addIdentifier('bornAt', 'date', ['label' => 'Born Date', 'format' => 'd/m/y'])
                ->addIdentifier('diedAt', 'date', ['label' => 'Died Date', 'format' => 'd/m/y'])
                ->add('_action', 'actions', ['actions' => ['edit' => [], 'delete' => []]]);
    }
}

