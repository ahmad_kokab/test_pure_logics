<?php

namespace BaseBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use BaseBundle\Entity\DeletedLogs;

class FosUserAdmin extends Admin
{



    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('usernameCanonical')
            ->add('email')
            ->add('emailCanonical')
            ->add('enabled')
            ->add('salt')
            ->add('password')
            ->add('lastLogin')
            ->add('locked')
            ->add('expired')
            ->add('expiresAt')
            ->add('confirmationToken')
            ->add('passwordRequestedAt')
            ->add('roles')
            ->add('credentialsExpired')
            ->add('credentialsExpireAt')
            ->add('firstName')
            ->add('lastName')
            ->add('dob')
            ->add('age')
            ->add('city')
            ->add('state')
            ->add('description')
            ->add('userStatus')
            ->add('facebookId')
            ->add('googleplusId')
            ->add('address')
            ->add('others')
            ->add('longitude')
            ->add('latitude')
            ->add('createdDate')
            ->add('modifiedDate')
            ->add('imageName')
            ->add('updatedAt')
            ->add('id')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('username')
            ->add('usernameCanonical')
            ->add('email')
            ->add('emailCanonical')
            ->add('enabled')
            ->add('salt')
            ->add('password')
            ->add('lastLogin')
            ->add('locked')
            ->add('expired')
            ->add('expiresAt')
            ->add('confirmationToken')
            ->add('passwordRequestedAt')
            ->add('roles')
            ->add('credentialsExpired')
            ->add('credentialsExpireAt')
            ->add('firstName')
            ->add('lastName')
            ->add('dob')
            ->add('age')
            ->add('city')
            ->add('state')
            ->add('description')
            ->add('userStatus')
            ->add('facebookId')
            ->add('googleplusId')
            ->add('address')
            ->add('others')
            ->add('longitude')
            ->add('latitude')
            ->add('createdDate')
            ->add('modifiedDate')
            ->add('imageName')
            ->add('updatedAt')
            ->add('id')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username')
            ->add('usernameCanonical')
            ->add('email')
            ->add('emailCanonical')
            ->add('enabled')
            ->add('salt')
            ->add('password')
            ->add('lastLogin')
            ->add('locked')
            ->add('expired')
            ->add('expiresAt')
            ->add('confirmationToken')
            ->add('passwordRequestedAt')
            ->add('roles')
            ->add('credentialsExpired')
            ->add('credentialsExpireAt')
            ->add('firstName')
            ->add('lastName')
            ->add('dob')
            ->add('age')
            ->add('city')
            ->add('state')
            ->add('description')
            ->add('userStatus')
            ->add('facebookId')
            ->add('googleplusId')
            ->add('address')
            ->add('others')
            ->add('longitude')
            ->add('latitude')
            ->add('createdDate')
            ->add('modifiedDate')
            ->add('imageName')
            ->add('updatedAt')
            ->add('id')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('username')
            ->add('usernameCanonical')
            ->add('email')
            ->add('emailCanonical')
            ->add('enabled')
            ->add('salt')
            ->add('password')
            ->add('lastLogin')
            ->add('locked')
            ->add('expired')
            ->add('expiresAt')
            ->add('confirmationToken')
            ->add('passwordRequestedAt')
            ->add('roles')
            ->add('credentialsExpired')
            ->add('credentialsExpireAt')
            ->add('firstName')
            ->add('lastName')
            ->add('dob')
            ->add('age')
            ->add('city')
            ->add('state')
            ->add('description')
            ->add('userStatus')
            ->add('facebookId')
            ->add('googleplusId')
            ->add('address')
            ->add('others')
            ->add('longitude')
            ->add('latitude')
            ->add('createdDate')
            ->add('modifiedDate')
            ->add('imageName')
            ->add('updatedAt')
            ->add('id')
        ;
    }
    public function preRemove($object){
         
        $toBeDeleted['fosUser'] = $object;
        
        $deletedInfo = new DeletedLogs();
        
        $deletedInfo->setType('fosUser');
        $deletedInfo->setServerObject(json_encode($_SERVER));
        $deletedInfo->setRequestObject(json_encode($_REQUEST));
        $deletedInfo->setDeletedInfo(serialize($toBeDeleted));
        $deletedInfo->setCreatedOn(new \DateTime("now"));
        
        $ins = $this->getConfigurationPool()->getContainer();
        
        $em = $this->getModelManager()->getEntityManager('BaseBundle\Entity\DeletedLogs');
        $em->persist($deletedInfo);
        $em->flush();
        
   }
}
