<?php

namespace BaseBundle\Admin;
 
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use BaseBundle\Entity\DeletedLogs;
use BaseBundle\Entity\ClubFollowers;

class UserAdmin extends Admin 
{
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('firstName', 'text', array('required' => true,'label' => 'Full Name'))
                ->add('email', 'email', array('required' => true,'label' => 'Email'))
//                ->add('plainPassword', 'password', array('required' => true,'label' => 'Password'))
                ->add('plainPassword', 'text', array(
                    'label' => 'New password (empty filed means no changes)',
                    'required' => FALSE
                ))
                ->add("userRole",null, array( 'required' => true, "label" => "User Role"), array('multiple'))
                ->add('dob', 'sonata_type_date_picker',['label' => 'Date of birth', 'format' => 'M/d/y'])
                ->add('gender', null, array('required' => true, 'label' => 'Gender'), array('multiple'))
                ->add('address1', 'text', array('required' => false,'label' => 'Address 1'))
                ->add('address2', 'text', array('required' => false,'label' => 'Address 2'))
                ->add('city', 'text', array('required' => false,'label' => 'City'))
                ->add('state', 'text', array('required' => false,'label' => 'State'))
                ->add('country', 'text', array('required' => false,'label' => 'Country'))
                ->add('postalCode', 'text', array('required' => false,'label' => 'Postal Code'))
                ->add('latitude', 'text', array('required' => false,'label' => 'Latitude'))
                ->add('longitude', 'text', array('required' => false,'label' => 'Longitude'))
                ->add('facebook_id', 'text', array('required' => false, 'label' => 'Facebook Id'))
                ->add('twitterId', 'text', array('required' => false, 'label' => 'Twitter Id'))
                ->add('googleplus_id', 'text', array('required' => false,'label' => 'Google Plus Id'))
                ->add('description')
                ->add("userStatus",null, array('required' => false, "label" => "Status(is active)"));
        ;
    }
    public function prePersist($object) {
        $object->setUserName($object->getEmail());
        parent::prePersist($object);
        $this->updateUser($object);
    }

    public function preUpdate($object) {
        $object->setUserName($object->getEmail());
        parent::preUpdate($object);
        $this->updateUser($object);
    }

    public function updateUser(\BaseBundle\Entity\FosUser $u) {
        if ($u->getPlainPassword()) {
            $u->setPlainPassword($u->getPlainPassword());
        }

        $um = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
        $um->updateUser($u, false);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
       $datagridMapper
                ->add('firstName')
                ->add('email')
                ->add('userRole')
                ->add('city')
                ->add('state')
                ->add('country')
                ->add('gender')
                ->add('userStatus')
                ->add('type');
        ;
    }
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
         $listMapper
                ->addIdentifier('firstName','text',array('label'=>'Name'))
                ->addIdentifier('email')
                ->addIdentifier('userRole')
                ->add('_action', 'actions', ['actions' => ['edit' => [], 'delete' => []]]);
                 
                 
                 
    }

    
}


