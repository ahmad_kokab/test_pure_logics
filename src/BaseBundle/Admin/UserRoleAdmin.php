<?php

namespace BaseBundle\Admin;
 
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use BaseBundle\Entity\DeletedLogs;
 //role_type
class UserRoleAdmin extends Admin
{
    
    protected $formOptions= ['validation_groups' =>['admin']];

// Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        if(!$this->isChild()) {
           
          //  $formMapper->add('locale', 'sonata_type_model', array(), array('multiple'));
            
        }
        
        $formMapper
               
                ->add('roleType', 'text', array('label' => 'Name'))
               // ->add('frequency_sort_order', 'text', array('label' => 'Sort Order'))
                //->add("is_active",null, array("label" => "Status(is active)"))
               
                
                ->end()
        ;
        
       
    }
 
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
       
        $datagridMapper
               ->add('roleType')
             //  ->add('frequency_sort_order')
              // ->add('locale')
              // ->add("is_active")
        ;
    }
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
         $listMapper
                ->addIdentifier('roleType')
                
            //    ->addIdentifier('frequency_sort_order')
                //->addIdentifier('is_active') 
            //      ->addIdentifier('locale')
            //    ->addIdentifier('frequency_created_date', 'date', ['label' => 'Creation Date', 'format' => 'd/m/y'])
            //    ->addIdentifier('frequency_modified_date', 'datetime', ['label' => 'Modification Date', 'format' => 'd/m/y'])
                ->add('_action', 'actions', ['actions' => ['edit' => [], 'delete' => []]]);
    }
    public function preRemove($object){
         
        $toBeDeleted['userRole'] = $object;
        
        $deletedInfo = new DeletedLogs();
        
        $deletedInfo->setType('UserRole');
        $deletedInfo->setServerObject(json_encode($_SERVER));
        $deletedInfo->setRequestObject(json_encode($_REQUEST));
        $deletedInfo->setDeletedInfo(serialize($toBeDeleted));
        $deletedInfo->setCreatedOn(new \DateTime("now"));
        
        $ins = $this->getConfigurationPool()->getContainer();
        
        $em = $this->getModelManager()->getEntityManager('BaseBundle\Entity\DeletedLogs');
        $em->persist($deletedInfo);
        $em->flush();
        
   }
}

