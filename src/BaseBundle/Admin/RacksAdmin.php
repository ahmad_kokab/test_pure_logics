<?php

namespace BaseBundle\Admin;
 
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
 
class RacksAdmin extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
               
                ->add('name', 'text', array('label' => 'Name','required'=>true))
                ->end()
        ;
    }
       

 
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
       
         $datagridMapper
               ->add('name')
        ;
    }
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
          $listMapper
                ->addIdentifier('name')
                ->add('_action', 'actions', ['actions' => ['edit' => [], 'delete' => []]]);
    }
}

