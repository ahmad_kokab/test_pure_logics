<?php

namespace BaseBundle\Admin;
 
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use BaseBundle\Entity\DeletedLogs;
 
class GenderAdmin extends Admin
{
    
    protected $formOptions= ['validation_groups' =>['admin']];

// Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        
        if(!$this->isChild()) {
           
          //  $formMapper->add('locale', 'sonata_type_model', array(), array('multiple'));
            
        }
        
        $formMapper
               
                ->add('genderType', 'text', array('label' => 'Name'))
                ->end()
        ;
    }
       

 
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
       
         $datagridMapper
               ->add('genderType')
             //  ->add('frequency_sort_order')
              // ->add('locale')

        ;
    }
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
          $listMapper
                ->addIdentifier('genderType')
                
            //    ->addIdentifier('frequency_sort_order')
             //   ->addIdentifier('is_active') 
            //      ->addIdentifier('locale')
            //    ->addIdentifier('frequency_created_date', 'date', ['label' => 'Creation Date', 'format' => 'd/m/y'])
            //    ->addIdentifier('frequency_modified_date', 'datetime', ['label' => 'Modification Date', 'format' => 'd/m/y'])
                ->add('_action', 'actions', ['actions' => ['edit' => [], 'delete' => []]]);
    }
    public function preRemove($object){
         
        $toBeDeleted['gender'] = $object;
        
        $deletedInfo = new DeletedLogs();
        
        $deletedInfo->setType('Gender');
        $deletedInfo->setServerObject(json_encode($_SERVER));
        $deletedInfo->setRequestObject(json_encode($_REQUEST));
        $deletedInfo->setDeletedInfo(serialize($toBeDeleted));
        $deletedInfo->setCreatedOn(new \DateTime("now"));
        
        $ins = $this->getConfigurationPool()->getContainer();
        
        $em = $this->getModelManager()->getEntityManager('BaseBundle\Entity\DeletedLogs');
        $em->persist($deletedInfo);
        $em->flush();
        
   }
}

