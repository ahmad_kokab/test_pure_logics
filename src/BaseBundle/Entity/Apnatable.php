<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Apnatable
 */
class Apnatable
{
    /**
     * @var integer
     */
    private $bxcdsc;

    /**
     * @var integer
     */
    private $x;

    /**
     * @var integer
     */
    private $c;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set bxcdsc
     *
     * @param integer $bxcdsc
     * @return Apnatable
     */
    public function setBxcdsc($bxcdsc)
    {
        $this->bxcdsc = $bxcdsc;

        return $this;
    }

    /**
     * Get bxcdsc
     *
     * @return integer 
     */
    public function getBxcdsc()
    {
        return $this->bxcdsc;
    }

    /**
     * Set x
     *
     * @param integer $x
     * @return Apnatable
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return integer 
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set c
     *
     * @param integer $c
     * @return Apnatable
     */
    public function setC($c)
    {
        $this->c = $c;

        return $this;
    }

    /**
     * Get c
     *
     * @return integer 
     */
    public function getC()
    {
        return $this->c;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
