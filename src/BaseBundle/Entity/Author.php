<?php

namespace BaseBundle\Entity;

/**
 * Author
 */
class Author
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $bornAt;

    /**
     * @var \DateTime
     */
    private $diedAt;

    /**
     * @var integer
     */
    private $id;

    public function __toString()
    {
        return $this->name ? $this->name : 'New Authors';
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Author
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set bornAt
     *
     * @param \DateTime $bornAt
     *
     * @return Author
     */
    public function setBornAt($bornAt)
    {
        $this->bornAt = $bornAt;

        return $this;
    }

    /**
     * Get bornAt
     *
     * @return \DateTime
     */
    public function getBornAt()
    {
        return $this->bornAt;
    }

    /**
     * Set diedAt
     *
     * @param \DateTime $diedAt
     *
     * @return Author
     */
    public function setDiedAt($diedAt)
    {
        $this->diedAt = $diedAt;

        return $this;
    }

    /**
     * Get diedAt
     *
     * @return \DateTime
     */
    public function getDiedAt()
    {
        return $this->diedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
