<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cars
 */
class Cars
{
    /**
     * @var string
     */
    private $number;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BaseBundle\Entity\CarModel
     */
    private $model;

    /**
     * @var \BaseBundle\Entity\Driver
     */
    private $driver;

    /**
     * @var \BaseBundle\Entity\CarClass
     */
    private $class;

    function __toString()
    {
        return $this->number ? $this->number.' '.$this->model : 'New Car';
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Cars
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set model
     *
     * @param \BaseBundle\Entity\CarModel $model
     * @return Cars
     */
    public function setModel(\BaseBundle\Entity\CarModel $model = null)
    {
        $this->model = $model;
    
        return $this;
    }

    /**
     * Get model
     *
     * @return \BaseBundle\Entity\CarModel
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set driver
     *
     * @param \BaseBundle\Entity\Driver $driver
     * @return Cars
     */
    public function setDriver(\BaseBundle\Entity\Driver $driver = null)
    {
        $this->driver = $driver;
    
        return $this;
    }

    /**
     * Get driver
     *
     * @return \BaseBundle\Entity\Driver
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Set class
     *
     * @param \BaseBundle\Entity\CarClass $class
     * @return Cars
     */
    public function setClass(\BaseBundle\Entity\CarClass $class = null)
    {
        $this->class = $class;
    
        return $this;
    }

    /**
     * Get class
     *
     * @return \BaseBundle\Entity\CarClass
     */
    public function getClass()
    {
        return $this->class;
    }
}
