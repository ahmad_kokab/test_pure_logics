<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarClass
 */
class CarClass
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $niceName;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set name
     *
     * @param string $name
     * @return CarClass
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set niceName
     *
     * @param string $niceName
     * @return CarClass
     */
    public function setNiceName($niceName)
    {
        $this->niceName = $niceName;
    
        return $this;
    }

    /**
     * Get niceName
     *
     * @return string 
     */
    public function getNiceName()
    {
        return $this->niceName;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
