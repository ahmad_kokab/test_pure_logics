<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gender
 */
class Gender
{
    /**
     * @var string
     */
    private $genderType;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set genderType
     *
     * @param string $genderType
     * @return Gender
     */
    public function setGenderType($genderType)
    {
        $this->genderType = $genderType;
    
        return $this;
    }

    /**
     * Get genderType
     *
     * @return string 
     */
    public function getGenderType()
    {
        return $this->genderType;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /*
    * @return string
    */
    public function __toString() {
        return (string) $this->genderType;
    }
}
