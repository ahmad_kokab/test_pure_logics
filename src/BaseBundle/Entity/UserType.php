<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserType
 */
class UserType
{
    /**
     * @var string
     */
    private $userType;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set userType
     *
     * @param string $userType
     * @return UserType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    
        return $this;
    }

    /**
     * Get userType
     *
     * @return string 
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /*
    * @return string
    */
    public function __toString() {
        return (string) $this->userType;
    }
}
