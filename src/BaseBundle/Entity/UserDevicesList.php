<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserDevicesList
 */
class UserDevicesList
{
    /**
     * @var string
     */
    private $deviceType;

    /**
     * @var string
     */
    private $deviceId;

    /**
     * @var string
     */
    private $aditional;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdOn;

    /**
     * @var \DateTime
     */
    private $modifiedOn;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BaseBundle\Entity\FosUser
     */
    private $user;


    /**
     * Set deviceType
     *
     * @param string $deviceType
     * @return UserDevicesList
     */
    public function setDeviceType($deviceType)
    {
        $this->deviceType = $deviceType;
    
        return $this;
    }

    /**
     * Get deviceType
     *
     * @return string 
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * Set deviceId
     *
     * @param string $deviceId
     * @return UserDevicesList
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
    
        return $this;
    }

    /**
     * Get deviceId
     *
     * @return string 
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Set aditional
     *
     * @param string $aditional
     * @return UserDevicesList
     */
    public function setAditional($aditional)
    {
        $this->aditional = $aditional;
    
        return $this;
    }

    /**
     * Get aditional
     *
     * @return string 
     */
    public function getAditional()
    {
        return $this->aditional;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return UserDevicesList
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return UserDevicesList
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    
        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set modifiedOn
     *
     * @param \DateTime $modifiedOn
     * @return UserDevicesList
     */
    public function setModifiedOn($modifiedOn)
    {
        $this->modifiedOn = $modifiedOn;
    
        return $this;
    }

    /**
     * Get modifiedOn
     *
     * @return \DateTime 
     */
    public function getModifiedOn()
    {
        return $this->modifiedOn;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \BaseBundle\Entity\FosUser $user
     * @return UserDevicesList
     */
    public function setUser(\BaseBundle\Entity\FosUser $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \BaseBundle\Entity\FosUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
