<?php

namespace BaseBundle\Entity;

/**
 * Books
 */
class Books
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTime
     */
    private $publishedYear;

    /**
     * @var \DateTime
     */
    private $createdOn;

    /**
     * @var \DateTime
     */
    private $modifiedOn;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BaseBundle\Entity\Racks
     */
    private $rack;

    /**
     * @var \BaseBundle\Entity\Author
     */
    private $author;

    public function __toString()
    {
        return $this->title ? $this->title : 'New Book';
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Books
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set publishedYear
     *
     * @param \DateTime $publishedYear
     *
     * @return Books
     */
    public function setPublishedYear($publishedYear)
    {
        $this->publishedYear = $publishedYear;

        return $this;
    }

    /**
     * Get publishedYear
     *
     * @return \DateTime
     */
    public function getPublishedYear()
    {
        return $this->publishedYear;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     *
     * @return Books
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set modifiedOn
     *
     * @param \DateTime $modifiedOn
     *
     * @return Books
     */
    public function setModifiedOn($modifiedOn)
    {
        $this->modifiedOn = $modifiedOn;

        return $this;
    }

    /**
     * Get modifiedOn
     *
     * @return \DateTime
     */
    public function getModifiedOn()
    {
        return $this->modifiedOn;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rack
     *
     * @param \BaseBundle\Entity\Racks $rack
     *
     * @return Books
     */
    public function setRack(\BaseBundle\Entity\Racks $rack = null)
    {
        $this->rack = $rack;

        return $this;
    }

    /**
     * Get rack
     *
     * @return \BaseBundle\Entity\Racks
     */
    public function getRack()
    {
        return $this->rack;
    }

    /**
     * Set author
     *
     * @param \BaseBundle\Entity\Author $author
     *
     * @return Books
     */
    public function setAuthor(\BaseBundle\Entity\Author $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \BaseBundle\Entity\Author
     */
    public function getAuthor()
    {
        return $this->author;
    }
}

