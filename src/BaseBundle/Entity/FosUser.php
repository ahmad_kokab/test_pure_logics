<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * FosUser
 */
class FosUser extends BaseUser
{
    public function __construct()
    {
        parent::__construct();
        $arr=['ROLE_USERS'];
        $this->setRoles($arr);
        $this->created_date= $this->modified_date = new \DateTime("now");
        $this->is_active=true;
    }

    public function __toString(){
        if($this->username != '') {
            return (string)$this->firstName != '' ?  $this->firstName. ' | ' . $this->username : $this->username;
        }else{
            return (string) 'Create User';
        }
    }

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $usernameCanonical;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $emailCanonical;

    /**
     * @var boolean
     */
    protected $enabled;

    /**
     * @var string
     */
    protected $salt;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var \DateTime
     */
    protected $lastLogin;

    /**
     * @var boolean
     */
    protected $locked;

    /**
     * @var boolean
     */
    protected $expired;

    /**
     * @var \DateTime
     */
    protected $expiresAt;

    /**
     * @var string
     */
    protected $confirmationToken;

    /**
     * @var \DateTime
     */
    protected $passwordRequestedAt;

    /**
     * @ORM\OneToOne(targetEntity="BaseBundle\Entity\UserRole")
     * @ORM\JoinColumn(name="user_role_id", referencedColumnName="id")
     */
    protected $role;

    /**
     * @var boolean
     */
    protected $credentialsExpired;

    /**
     * @var \DateTime
     */
    protected $credentialsExpireAt;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var \DateTime
     */
    protected $dob;

    /**
     * @var string
     */
    protected $age;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $state;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var boolean
     */
    protected $userStatus;

    /**
     * @var string
     */
    protected $facebookId;
    public function serialize()
    {
        return serialize(array($this->facebookId, parent::serialize()));
    }

    public function unserialize($data)
    {
        list($this->facebookId, $parentData) = unserialize($data);
        parent::unserialize($parentData);
    }
    /**
     * @var string
     */
    protected $googleplusId;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $longitude;

    /**
     * @var string
     */
    protected $latitude;

    /**
     * @var \DateTime
     */
    protected $createdDate;

    /**
     * @var \DateTime
     */
    protected $modifiedDate;

    /**
     * @var string
     */
    protected $imageName;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @var integer
     */
    protected $id;

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @var \BaseBundle\Entity\UserType
     */
    protected $type;

    /**
     * @var \BaseBundle\Entity\UserRole
     */
    protected $userRole;

    /**
     * @var \BaseBundle\Entity\Gender
     */
    protected $gender;

    /**
     * @return \DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param string $facebookId
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    /**
     * @return string
     */
    public function getGoogleplusId()
    {
        return $this->googleplusId;
    }

    /**
     * @param string $googleplusId
     */
    public function setGoogleplusId($googleplusId)
    {
        $this->googleplusId = $googleplusId;
    }

    /**
     * @return boolean
     */
    public function isUserStatus()
    {
        return $this->userStatus;
    }

    /**
     * @param boolean $userStatus
     */
    public function setUserStatus($userStatus)
    {
        $this->userStatus = $userStatus;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return Gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param Gender $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @param \DateTime $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @var \BaseBundle\Entity\WsbUserFrequency
     */
    protected $userFrequency;

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get first_name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    /**
     * Set role
     *
     * @param \BaseBundle\Entity\UserRole $role
     * @return User
     */
    public function setRole(\BaseBundle\Entity\UserRole $role = null)
    {
        $this->role = $role;
        $array1=['ROLE_USER'];
        $array2=['ROLE_ADMIN'];
        $array3=['ROLE_SUPER_ADMIN'];
        if($role== 'ROLE_ADMIN'){
            $this->setRoles($array2);

        }
        elseif($role== 'ROLE_USER'){
            $this->setRoles($array1);
        }
        elseif($role == 'ROLE_SUPER_ADMIN'){
            $this->setRoles($array3);
        }
        return $this;
    }

    /**
     * Get role
     *
     * @return \BaseBundle\Entity\UserRole
     */
    public function getRole()
    {
        return $this->role;
    }


    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return FosUser
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Get expired
     *
     * @return boolean 
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime 
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Get credentialsExpired
     *
     * @return boolean 
     */
    public function getCredentialsExpired()
    {
        return $this->credentialsExpired;
    }

    /**
     * Get credentialsExpireAt
     *
     * @return \DateTime 
     */
    public function getCredentialsExpireAt()
    {
        return $this->credentialsExpireAt;
    }

    /**
     * Set age
     *
     * @param string $age
     * @return FosUser
     */
    public function setAge($age)
    {
        $this->age = $age;
    
        return $this;
    }

    /**
     * Get age
     *
     * @return string 
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Get userStatus
     *
     * @return boolean 
     */
    public function getUserStatus()
    {
        return $this->userStatus;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return FosUser
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return FosUser
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
    
        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return FosUser
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set type
     *
     * @param \BaseBundle\Entity\UserType $type
     * @return FosUser
     */
    public function setType(\BaseBundle\Entity\UserType $type = null)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return \BaseBundle\Entity\UserType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set userRole
     *
     * @param \BaseBundle\Entity\UserRole $userRole
     * @return FosUser
     */
    public function setUserRole(\BaseBundle\Entity\UserRole $userRole = null)
    {

        $this->setRoles([$userRole]);
        $this->userRole = $userRole;
    
        return $this;
    }

    /**
     * Get userRole
     *
     * @return \BaseBundle\Entity\UserRole
     */
    public function getUserRole()
    {
        return $this->userRole;
    }

    /**
     * Set userFrequency
     *
     * @param \BaseBundle\Entity\WsbUserFrequency $userFrequency
     * @return FosUser
     */
    public function setUserFrequency(\BaseBundle\Entity\WsbUserFrequency $userFrequency = null)
    {
        $this->userFrequency = $userFrequency;
    
        return $this;
    }

    /**
     * Get userFrequency
     *
     * @return \BaseBundle\Entity\WsbUserFrequency
     */
    public function getUserFrequency()
    {
        return $this->userFrequency;
    }
    /**
     * @var string
     */
    private $others;


    /**
     * Set others
     *
     * @param string $others
     * @return FosUser
     */
    public function setOthers($others)
    {
        $this->others = $others;
    
        return $this;
    }

    /**
     * Get others
     *
     * @return string 
     */
    public function getOthers()
    {
        return $this->others;
    }

    /**
     * @param Array
     */
    public function setFBData($fbdata)
    {
        if (isset($fbdata['id'])) {
            $this->setFacebookId($fbdata['id']);
            $this->addRole('ROLE_FACEBOOK');
        }
        if (isset($fbdata['first_name'])) {
            $this->setFirstname($fbdata['first_name']);
        }
        if (isset($fbdata['last_name'])) {
            $this->setLastname($fbdata['last_name']);
        }
        if (isset($fbdata['email'])) {
            $this->setEmail($fbdata['email']);
        }
    }
    /**
     * @var \BaseBundle\Entity\UserImage
     */
    private $profileImage;

    /**
     * @var \BaseBundle\Entity\UserImage
     */
    private $coverImage;


    /**
     * Set profileImage
     *
     * @param \BaseBundle\Entity\UserImage $profileImage
     * @return FosUser
     */
    public function setProfileImage(\BaseBundle\Entity\UserImage $profileImage = null)
    {
        $this->profileImage = $profileImage;

        return $this;
    }

    /**
     * Get profileImage
     *
     * @return \BaseBundle\Entity\UserImage
     */
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * Set coverImage
     *
     * @param \BaseBundle\Entity\UserImage $coverImage
     * @return FosUser
     */
    public function setCoverImage(\BaseBundle\Entity\UserImage $coverImage = null)
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    /**
     * Get coverImage
     *
     * @return \BaseBundle\Entity\UserImage
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }
    /**
     * @var string
     */
    private $postalCode;


    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return FosUser
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    
        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }
    /**
     * @var string
     */
    private $country;


    /**
     * Set country
     *
     * @param string $country
     * @return FosUser
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }
    /**
     * @var string
     */
    private $twitterId;

    /**
     * @var string
     */
    private $address1;

    /**
     * @var string
     */
    private $address2;


    /**
     * Set twitterId
     *
     * @param string $twitterId
     * @return FosUser
     */
    public function setTwitterId($twitterId)
    {
        $this->twitterId = $twitterId;
    
        return $this;
    }

    /**
     * Get twitterId
     *
     * @return string 
     */
    public function getTwitterId()
    {
        return $this->twitterId;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return FosUser
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    
        return $this;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return FosUser
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    
        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }
    /**
     * @var string
     */
    private $confirmationCode;


    /**
     * Set confirmationCode
     *
     * @param string $confirmationCode
     * @return FosUser
     */
    public function setConfirmationCode($confirmationCode)
    {
        $this->confirmationCode = $confirmationCode;
    
        return $this;
    }

    /**
     * Get confirmationCode
     *
     * @return string 
     */
    public function getConfirmationCode()
    {
        return $this->confirmationCode;
    }
    /**
     * @var integer
     */
    private $userFrequencyId;


    /**
     * Set userFrequencyId
     *
     * @param integer $userFrequencyId
     * @return FosUser
     */
    public function setUserFrequencyId($userFrequencyId)
    {
        $this->userFrequencyId = $userFrequencyId;
    
        return $this;
    }

    /**
     * Get userFrequencyId
     *
     * @return integer 
     */
    public function getUserFrequencyId()
    {
        return $this->userFrequencyId;
    }
}
