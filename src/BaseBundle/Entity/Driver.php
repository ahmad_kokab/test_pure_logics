<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Driver
 */
class Driver
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BaseBundle\Entity\FosUser
     */
    private $user;

    function __toString()
    {
        return $this->name ? $this->name : 'New Driver';
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Driver
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \BaseBundle\Entity\FosUser $user
     * @return Driver
     */
    public function setUser(\BaseBundle\Entity\FosUser $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \BaseBundle\Entity\FosUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
