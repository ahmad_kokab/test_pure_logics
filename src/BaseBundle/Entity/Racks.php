<?php

namespace BaseBundle\Entity;

/**
 * Racks
 */
class Racks
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $id;

    public function __toString()
    {
        return $this->name ? $this->name : 'New Rack';
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Racks
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
