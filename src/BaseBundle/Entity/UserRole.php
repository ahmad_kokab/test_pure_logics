<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserRole
 */
class UserRole
{
    /**
     * @var string
     */
    private $roleType;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set roleType
     *
     * @param string $roleType
     * @return UserRole
     */
    public function setRoleType($roleType)
    {
        $this->roleType = $roleType;
    
        return $this;
    }

    /**
     * Get roleType
     *
     * @return string 
     */
    public function getRoleType()
    {
        return $this->roleType;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /*
    * @return string
    */
    public function __toString() {
        return (string) $this->roleType;
    }
}
