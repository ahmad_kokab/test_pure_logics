<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarModel
 */
class CarModel
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $title;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BaseBundle\Entity\CarMake
     */
    private $make;

    public function __toString()
    {
        return $this->title ? $this->title.' - '.$this->make->getTitle() : 'New model';
    }

    /**
     * Set code
     *
     * @param string $code
     * @return CarModel
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CarModel
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set make
     *
     * @param \BaseBundle\Entity\CarMake $make
     * @return CarModel
     */
    public function setMake(\BaseBundle\Entity\CarMake $make = null)
    {
        $this->make = $make;
    
        return $this;
    }

    /**
     * Get make
     *
     * @return \BaseBundle\Entity\CarMake
     */
    public function getMake()
    {
        return $this->make;
    }
}
