<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 */
class Client
{
    /**
     * @var string
     */
    private $randomId;

    /**
     * @var string
     */
    private $redirectUris;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var string
     */
    private $allowedGrantTypes;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set randomId
     *
     * @param string $randomId
     * @return Client
     */
    public function setRandomId($randomId)
    {
        $this->randomId = $randomId;
    
        return $this;
    }

    /**
     * Get randomId
     *
     * @return string 
     */
    public function getRandomId()
    {
        return $this->randomId;
    }

    /**
     * Set redirectUris
     *
     * @param string $redirectUris
     * @return Client
     */
    public function setRedirectUris($redirectUris)
    {
        $this->redirectUris = $redirectUris;
    
        return $this;
    }

    /**
     * Get redirectUris
     *
     * @return string 
     */
    public function getRedirectUris()
    {
        return $this->redirectUris;
    }

    /**
     * Set secret
     *
     * @param string $secret
     * @return Client
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    
        return $this;
    }

    /**
     * Get secret
     *
     * @return string 
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Set allowedGrantTypes
     *
     * @param string $allowedGrantTypes
     * @return Client
     */
    public function setAllowedGrantTypes($allowedGrantTypes)
    {
        $this->allowedGrantTypes = $allowedGrantTypes;
    
        return $this;
    }

    /**
     * Get allowedGrantTypes
     *
     * @return string 
     */
    public function getAllowedGrantTypes()
    {
        return $this->allowedGrantTypes;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
