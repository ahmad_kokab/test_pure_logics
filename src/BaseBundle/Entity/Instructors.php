<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Instructors
 */
class Instructors
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $charges;

    /**
     * @var string
     */
    private $web;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $facebook;

    /**
     * @var string
     */
    private $googlePlus;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdOn;

    /**
     * @var \DateTime
     */
    private $modifiedOn;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BaseBundle\Entity\FosUser
     */
    private $user;


    /**
     * Set name
     *
     * @param string $name
     * @return Instructors
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Instructors
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set charges
     *
     * @param string $charges
     * @return Instructors
     */
    public function setCharges($charges)
    {
        $this->charges = $charges;
    
        return $this;
    }

    /**
     * Get charges
     *
     * @return string 
     */
    public function getCharges()
    {
        return $this->charges;
    }

    /**
     * Set web
     *
     * @param string $web
     * @return Instructors
     */
    public function setWeb($web)
    {
        $this->web = $web;
    
        return $this;
    }

    /**
     * Get web
     *
     * @return string 
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Instructors
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Instructors
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Instructors
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    
        return $this;
    }

    /**
     * Get facebook
     *
     * @return string 
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set googlePlus
     *
     * @param string $googlePlus
     * @return Instructors
     */
    public function setGooglePlus($googlePlus)
    {
        $this->googlePlus = $googlePlus;
    
        return $this;
    }

    /**
     * Get googlePlus
     *
     * @return string 
     */
    public function getGooglePlus()
    {
        return $this->googlePlus;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Instructors
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return Instructors
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    
        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set modifiedOn
     *
     * @param \DateTime $modifiedOn
     * @return Instructors
     */
    public function setModifiedOn($modifiedOn)
    {
        $this->modifiedOn = $modifiedOn;
    
        return $this;
    }

    /**
     * Get modifiedOn
     *
     * @return \DateTime 
     */
    public function getModifiedOn()
    {
        return $this->modifiedOn;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \BaseBundle\Entity\FosUser $user
     * @return Instructors
     */
    public function setUser(\BaseBundle\Entity\FosUser $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \BaseBundle\Entity\FosUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
