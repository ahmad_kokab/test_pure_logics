<?php

namespace BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Doctrine\ORM\EntityManager;
use BaseBundle\Entity;
use BaseBundle\Entity\WsbUserFriend;

class ApiController extends Controller
{

    /**
     * @Route("/signupuser")
     * @Template()
     */
    public function registerUserAction(Request $request)
    {
        //$username=$request->get('username');
        $email=$request->get('email');
        $password=$request->get('password');
        $name=$request->get('name');
        $error = 0;
        $response = '';
        $msg = '';
        /*if(!isset($username) || $username == ''){
            $error = 1;
            $msg = 'username missing';
        }*/
        if(!isset($email) || $email == ''){
            $error = 1;
            $msg = 'email missing';
        }
        if(!isset($password) || $password == ''){
            $error = 1;
            $msg = 'password missing';
        }
        $user_manager = $this->get('fos_user.user_manager');
        /*$user_info = $user_manager->findUserByUsername($username);
        if($user_info){
            $error = 1;
            $msg = 'username already in use';
        }*/
        $user_info = $user_manager->findUserByEmail($email);
        if($user_info){
            $error = 1;
            $msg = 'email already in use';
        }
        if($error == 0){
            $em = $this->getDoctrine()->getEntityManager();
            $repository = $em->getRepository('BaseBundle:UserType');
            $user_type= $repository->findOneBy(array('user_type' => 'web'));
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->createUser();
            $user->setUsername($email);
            $user->setEmail($email);
            $user->setPlainPassword($password);
            $user->setEnabled(true);
            $user->setType($user_type);
            $userManager->updateUser($user);
            $responseStatus=0;
            $user_info = $user_manager->findUserByEmail($email);
            $response = new Response(json_encode(array('user_info'=> array('user_id'=>$user_info->getId(), 'user_name' => $user_info->getUsername(), 'user_email'=>$user_info->getEmail()),'msg'=>'User registered successfully', 'response_status'=>$responseStatus)));
        }else{
            $responseStatus=1;
            $response = new Response(json_encode(array('user_info'=> array(),'msg'=>$msg, 'response_status'=>$responseStatus)));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/loginuser")
     * @Template()
     */
    public function loginUserAction(Request $request)
    {
        $email=$request->get('email');
        $password=$request->get('password');


        $user_manager = $this->get('fos_user.user_manager');
        $factory = $this->get('security.encoder_factory');
        $user = $user_manager->findUserByEmail($email);
        $response = '';
        if($user){
            $user_info = $user_manager->loadUserByUsername($user->getUsername());
            $encoder = $factory->getEncoder($user_info);
            $is_correct_pass = ($encoder->isPasswordValid($user_info->getPassword(),$password,$user_info->getSalt()) ? true : false);
            if($is_correct_pass){
                //$user_info = $user_manager->findUserByUsername($username);
                $responseStatus = 0;
                $response = new Response(json_encode(array('user_info'=> array('user_id'=>$user->getId(), 'user_name' => $user->getUsername(), 'user_email'=>$user->getEmail()), 'msg'=>'User Successfully Login', 'response_status'=>$responseStatus)));
            }else{
                $responseStatus=1;
                $response = new Response(json_encode(array('user_info'=> array(),'msg'=>'Incorrect password', 'response_status'=>$responseStatus)));
            }
        }else{
            $responseStatus=1;
            $response = new Response(json_encode(array('user_info'=> array(),'msg'=>'Unable to find this username', 'response_status'=>$responseStatus)));
        }


        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    /**
     * @Route("/getallsports")
     * @Template()
     */
    public function getAllSportsAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $repository = $em->getRepository('BaseBundle:WsbSport');
        $sports = $repository->findAll();
        //$sql = "SELECT * FROM wsb_sport";
        //$stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        //$stmt->execute();
        //$sports = $stmt->fetchAll(\PDO::FETCH_CLASS);
        //print_r($sports);exit;

        $sports_array = array();
        $msg = 'No record found';
        $responseStatus = 0;
        if($sports){
            foreach($sports as $sport){
                $sports_array[] = array("id"=>$sport->getId(),"name"=>$sport->getSportName());
            }
            $msg = 'success';
        }
        $response = new Response(json_encode(array('sports'=> $sports_array,'msg'=>$msg, 'response_status'=>$responseStatus)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/requestFriend")
     * @Template()
     */
    public function sendFriendRequestAction(Request $request)
    {
        $fid=$request->get('friend_id');
        $uid=$request->get('user_id');

        $em = $this->getDoctrine()->getEntityManager();
        $repository = $em->getRepository('BaseBundle:WsbUserFriend');
        //Getting User Repository
        $userRepo = $em->getRepository('BaseBundle:FosUser');

        $user_info = $userRepo->findOneById($uid);

        $friend_info = $userRepo->findOneById($fid);


        if(!empty($user_info) && !empty($friend_info)) {

            $user_type1= $repository->findOneBy(array('friend' => $fid,'user'=>$uid));
            $user_type2= $repository->findOneBy(array('user'=>$fid,'friend'=>$uid));



            if (!$user_type1 || !$user_type2) {

                $entityManager = $this->getDoctrine()->getEntityManager();
                $wsbuserfriend = new WsbUserFriend();
                $wsbuserfriend->setFriendId($fid);
                $wsbuserfriend->setFriend($user_info);
                $wsbuserfriend->setUserId($uid);
                $wsbuserfriend->setUser($friend_info);
                $wsbuserfriend->setIsFriend(0);
                $wsbuserfriend->setFriendSortOrder(0);
//            echo '<pre>'.print_r($wsbuserfriend,true).'</pre>'; die;
                $entityManager->persist($wsbuserfriend);
                $entityManager->flush();

                $responseStatus = 0;

                if ($user_info) {

                    $uinfo_array = array("id" => $user_info->getId(), "dob" => $user_info->getDob(), "city" => $user_info->getCity());

                    $msg = 'success';
                }


                if ($friend_info) {

                    $finfo_array = array("id" => $friend_info->getiD(), "dob" => $friend_info->getDob(), "city" => $friend_info->getCity());

                    $msg = 'success';
                }

                $response = new Response(json_encode(array('user' => $uinfo_array, 'friend' => $finfo_array, 'msg' => "success", 'response_status' => $responseStatus)));
                // $response = new Response(json_encode(array('uid'=> $uid,'fid'=> $fid,'msg'=>'Request sent successfully','id'=> $id)));
            } else {
                $responseStatus = 1;

                $response = new Response(json_encode(array('msg' => 'Friend Request Already sent', 'uid' => $uid, 'response_status' => $responseStatus)));
            }

        }else{
            $responseStatus = 0;
            $response = new Response(json_encode(array('msg' => 'One of user is invalid', 'uid' => $uid, 'response_status' => $responseStatus)));
        }
        return $response;
    }

    /**
     * @Route("/getfriendrequest")
     * @Template()
     */
    public function getFriendRequestAction(Request $request)
    {
        //User Id
        $uid=$request->get('user_id');

        //Entity Manager Get
        $em = $this->getDoctrine()->getEntityManager();

        //Get Repository of Freindship relations table
        $friendRequestRepo = $em->getRepository('BaseBundle:WsbUserFriend');

        $userRepo = $em->getRepository('BaseBundle:FosUser');

        $user_info = $userRepo->findOneById($uid);

        //List of All Friend Requests Against User Id
        $friend_req = $friendRequestRepo->findByFriend($user_info);
//        $friend_req = $friendRequestRepo->createQueryBuilder('uf')
//            ->Where('uf.friend = :user_id')
//            ->setParameter('user_id', $uid)
//            ->getQuery()->getResult();
        //1 Means which are accepted
        //0 Means which are not accepted

        //If there is any friend Request
        if($friend_req)
        {
            //Extractn User Ids from Friend Request
            $friends_ids = [];

            foreach($friend_req as $request){
                if($request->getIsFriend() == 1) {
                    $friends_ids[] = $request->getUser()->getId();
                }
            }
            //Get User Profiles Against their Ids
            //Get Entity Manager Again
            $em = $this->getDoctrine()->getEntityManager();

            //Get All Profiles
            $qb = $em->createQueryBuilder();
            $qb->select('userProfiles');
            $qb->from('BaseBundle:FosUser', 'userProfiles');
            $qb->where($qb->expr()->in('userProfiles.id', $friends_ids));

            //ArrayCollection
            $friends_info = $qb->getQuery()->getResult();

            $req_info = array();
            $msg = 'No record found';
            $responseStatus = 0;
            if($friends_info)
            {
                foreach($friends_info as $userinfo)
                {
                    $req_info[] = array("id"=>$userinfo->getId(),"name"=>$userinfo->getUserName());
                }

            }
            $msg = 'success';

            $responseStatus=1;
            $response = new Response(json_encode(array('user'=> $req_info,'msg'=>"success", 'response_status'=>$responseStatus)));

        }else{
            $responseStatus=0;
            $response = new Response(json_encode(array('msg'=>"Fail", 'response_status'=>$responseStatus)));
        }
        return $response;
    }


    /**
     * @Route("/rejectfriendrequest")
     * @Template()
     * @param Request $request default symfony request object
     * @return JSON json formate response
     */
    public function rejectFriendRequestAction(Request $request)
    {
        //Intializing Response Variables
        $status =   0;
        $msg    =   'Fail';

        //retriving Required data
        $id=$request->get('id');

        //If is valid required data
        if($id != 0){

            //Getting Object to be deleted out
            $em = $this->getDoctrine()->getEntityManager();
            $requestObj = $em->getRepository('BaseBundle:WsbUserFriend')->findOneBy(array('id'=>$id));

            //If these is no object with this id || Might be because record is already deleted
            if($requestObj == NULL){

                //Success Response but no object deleted
                $msg = 'Freind request already declined';
                $status = 1;

            }else{

                //Deleting Friend Request Object
                $em = $this->getDoctrine()->getEntityManager();
                $em->remove($requestObj);
                $em->flush();
                //Success Responce after deleting data
                $msg    =   'Request Declined';
                $status =   1;
            }
        }else{

            //Failed Missing Required Data
            $msg    =   'Required data is missing';
            $status =   0;

        }

        //Generating Response
        $response = new Response(json_encode(array('msg'=>$msg, 'response_status'=>$status)));

        return $response;

    }

    /**
     * @Route("/acceptFriendRequest")
     * @Template()
     * @param Request $request default symfony request object
     * @return JSON json formate response
     */
    public function acceptFriendRequestAction(Request $request)
    {
        //Intializing Response Variables
        $status =   0;
        $msg    =   'Fail';

        //retriving Required data
        $id=$request->get('id');

        //If is valid required data
        if($id != 0){

            //Getting Object to be deleted out
            $em = $this->getDoctrine()->getEntityManager();
            $requestObj = $em->getRepository('BaseBundle:WsbUserFriend')->findOneBy(array('id'=>$id));

            //If these is no object with this id || Might be because record is already deleted
            if($requestObj == NULL){

                //Success Response but no object deleted
                $msg = 'Freind request already declined';
                $status = 0;

            }else{

                //Updating Friend Request Object
                $em = $this->getDoctrine()->getEntityManager();

                //Updating Status
                $requestObj->setIsFriend(1);

                $em->flush();
                //Success Responce after updating record
                $msg    =   'Request Accepted';
                $status =   1;
            }
        }else{

            //Failed Missing Required Data
            $msg    =   'Required data is missing';
            $status =   0;

        }

        //Generating Response
        $response = new Response(json_encode(array('msg'=>$msg, 'response_status'=>$status)));

        return $response;

    }

    /**
     * @Route("/getSentFriendRequest")
     * @Template()
     * @param Request $request default symfony request object
     * @return JSON json formate response
     */
    public function getSentFriendRequestsAction(Request $request)
    {
        //Intializing Response Variables
        $status =   0;
        $msg    =   'Fail';
        $req_info = [];

        //Collecting Required Data
        $uid=$request->get('user_id');

        //If Required adata is valid
        if($uid != 0 && $uid != '' && $uid != NULL && $uid){

            //Entity Manager Get
            $em = $this->getDoctrine()->getEntityManager();

            //Get Repository of Freindship relations table
            $friendRequestRepo = $em->getRepository('BaseBundle:WsbUserFriend');

            //List of All Friend Requests Against User Id
            $userRepo = $em->getRepository('BaseBundle:FosUser');

            $user_info = $userRepo->findOneById($uid);

            $friend_req = $friendRequestRepo->findByUser($user_info);

            //If there is any friend Request
            if($friend_req)
            {
                //Extractn User Ids from Friend Request
                $friends_ids = [];

                foreach($friend_req as $request){

                    $friends_ids[] = $request->getFriend()->getId();

                }

                //Get User Profiles Against their Ids
                //Get Entity Manager Again
                $em = $this->getDoctrine()->getEntityManager();

                //Get All Profiles
                $qb = $em->createQueryBuilder();
                $qb->select('userProfiles');
                $qb->from('BaseBundle:FosUser', 'userProfiles');
                $qb->where($qb->expr()->in('userProfiles.id', $friends_ids));

                //ArrayCollection
                $friends_info = $qb->getQuery()->getResult();

                if($friends_info)
                {
                    foreach($friends_info as $userinfo)
                    {
                        $req_info[] = array(
                            "id"=>$userinfo->getId(),
                            "name"=>$userinfo->getUserName(),
                        );
                    }
                    $status =   1;
                    $msg    =   'Success';

                }else{
                    $status =   1;
                    $msg    =   'No Requests';
                }
            }else{
                $status =   1;
                $msg    =   'No Requests';
            }
        }else{
            $status =   0;
            $msg    =   'Missing Required data';
        }
        $response = new Response(json_encode(array('user'=> $req_info,'msg'=>$msg, 'response_status'=>$status)));

        return $response;
    }

    /**
     * @Route("/getAllFriends")
     * @Template()
     * @param Request $request default symfony request object
     * @return JSON json formate response
     */
    public function getAllFriendsAction(Request $request)
    {
        //Intializing Response Variables
        $status =   0;
        $msg    =   'Fail';
        $req_info = [];

        //Collecting Required Data
        $uid=$request->get('user_id');

        //If Required adata is valid
        if($uid != 0 && $uid != '' && $uid != NULL && $uid){

            //Entity Manager Get
            $em = $this->getDoctrine()->getEntityManager();

            //Get Repository of Freindship relations table
            $friendRequestRepo = $em->getRepository('BaseBundle:WsbUserFriend');

            $userRepo = $em->getRepository('BaseBundle:FosUser');

            $user_info = $userRepo->findOneById($uid);

            $friend_req1 = $friendRequestRepo->findByUser($user_info);
            $friend_req2 = $friendRequestRepo->findByFriend($user_info);

            //List of All Friend Requests Against User Id
            $friend_req = array_merge($friend_req1,$friend_req2);

            //If there is any friend Request
            if($friend_req)
            {
                //Extractn User Ids from Friend Request
                $friends_ids = [];

                foreach($friend_req as $request){

                    if($request->getIsFriend() == 1) {
                        if ($request->getFriendId() != $uid) {
                            $friends_ids[] = $request->getFriend()->getId();
                        }
                        if ($request->getUserId() != $uid) {
                            $friends_ids[] = $request->getUser()->getId();
                        }
                    }
                }

                //Get User Profiles Against their Ids
                //Get Entity Manager Again
                $em = $this->getDoctrine()->getEntityManager();

                //Get All Profiles
                $qb = $em->createQueryBuilder();
                $qb->select('userProfiles');
                $qb->from('BaseBundle:FosUser', 'userProfiles');
                $qb->where($qb->expr()->in('userProfiles.id', $friends_ids));

                //ArrayCollection
                $friends_info = $qb->getQuery()->getResult();

                if($friends_info)
                {
                    foreach($friends_info as $userinfo)
                    {
                        $req_info[] = array(
                            "id"=>$userinfo->getId(),
                            "name"=>$userinfo->getUserName(),
                        );
                    }
                    $status =   1;
                    $msg    =   'Success';

                }else{
                    $status =   1;
                    $msg    =   'No Friends';
                }
            }else{
                $status =   1;
                $msg    =   'No Friends';
            }
        }else{
            $status =   0;
            $msg    =   'Missing Required data';
        }
        $response = new Response(json_encode(array('user'=> $req_info,'msg'=>$msg, 'response_status'=>$status)));

        return $response;
    }

    /**
     * @Route("/editsignup")
     * @Template()
     * @param Request $request default symfony request object
     * @return JSON json formate response
     */
    public function editSignUpAction(Request $request)
    {
        $email="";
        $password="";
        $username="";
        $id=$request->get('id');
        $status=1;
        $em = $this->getDoctrine()->getEntityManager();
        $repository = $em->getRepository('BaseBundle:WsbUserFriend');
        $edit_user= $repository->findOneBy(array('id' => $id));

        if($id)
        {
            //die("Called");
            $email=$request->get('email');
            $password=$request->get('password');
            $name=$request->get('name');

            $em = $this->getDoctrine()->getEntityManager();
            $repository = $em->getRepository('BaseBundle:UserType');

            if(isset($email)){

                //die("Called");

//                    
//                           $user_type= $repository->findOneBy(array('user_type' => 'web'));
//                            $user_info= $userRepo->findOneById($uid);
//                           $userManager = $this->get('fos_user.user_manager');
//                           
//                           
//                           Set and Update Email??
//                           

                $em = $this->getDoctrine()->getEntityManager();
                $userRepo = $em->getRepository('BaseBundle:User');

                $user_info= $userRepo->findById($id);
                $req_info=array();
                print_r($req_info);
                //die("Called");
                foreach($user_info as $info){
                    $req_info[] = array(
                        "id"=>$info->getId(),
                        "name"=>$info->getUserName(),
                    );
                }






            }

        }
        $response = new Response(json_encode(array('user'=> $req_info,'msg'=>'Email updated', 'response_status'=>$status)));
        return $response;


    }

    /**
     * @Route("/blockfriend")
     * @Template()
     * @param Request $request default symfony request object
     * @return JSON json formate response
     */
    public function blockFriendAction(Request $request)
    {
        //Intializing Response Variables
        $status =   0;
        $msg    =   'Fail';
        $req_info = [];

        //Collecting Required Data
        $uid=$request->get('user_id');
        $bid=$request->get('blocked_user_id');

        //If Required adata is valid
        $isValidUserId = $uid != 0 && $uid != '' && $uid != NULL && $uid;
        $isValidBlockId = $bid != 0 && $bid != '' && $bid != NULL && $bid;

        if($isValidUserId && $isValidBlockId){

            //Entity Manager Get
            $em = $this->getDoctrine()->getEntityManager();

            //Creating Block Object
            $blockedObject = new \BaseBundle\Entity\BlockedUsers();
            $blockedObject->setUserId($bid);
            $blockedObject->setBlockedBy($uid);

            //Saving Object
            $em->persist($blockedObject);
            $em->flush();


            $status =   1;
            $msg    =   'Success';
        }else{
            $status =   0;
            $msg    =   'Missing Required data';
        }
        $response = new Response(json_encode(array('user'=> $req_info,'msg'=>$msg, 'response_status'=>$status)));

        return $response;

    }

    /**
     * @Route("/getuserprofileinfo")
     * @Template()
     * @param Request $request default symfony request object
     * @return JSON json formate response
     */
    public function getUserProfileInfoAction(Request $request)
    {
        //die("Called");
        //Intializing Response Variables
        $status =   0;
        $msg    =   'Fail';
        $uinfo_array = array();

        //retriving Required data
        $uid=$request->get('user_id');
        //Getting Required Data
        if($uid){

            //Getting Entity Manager
            $em = $this->getDoctrine()->getEntityManager();

            //Getting User Repository
            $userRepo = $em->getRepository('BaseBundle:FosUser');

            //Getting Data From User Repository
            $user_info= $userRepo->findOneById($uid);

            //If User Info is Available
            if($user_info){

                $uinfo_array = array("id"=>$user_info->getId(),"dob"=>$user_info->getDob(),"city"=>$user_info->getCity());
                $status =   1;
                $msg    =   'Success';

            }else{
                $status =   1;
                $msg    =   'Profile Not Found';
            }


        }else{
            $status =   0;
            $msg    =   'Missing Required data';
        }
        $response = new Response(json_encode(array('user'=> $uinfo_array,'msg'=>$msg, 'response_status'=>$status)));
        return $response;
    }


}
