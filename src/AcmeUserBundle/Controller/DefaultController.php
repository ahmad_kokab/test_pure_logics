<?php

namespace AcmeUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AcmeUserBundle:Default:index.html.twig', array('name' => $name));
    }

    public function authorityCheckAction(){

        //check for user login status
        $user = $this->container->get('security.context')->getToken()->getUser();

        $router = $this->container->get('router');

        if (
            $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')
            ||
            $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')
        ) {

            if (
                $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')
            ) {

                return new RedirectResponse($router->generate('sonata_admin_redirect'));

            }else{

                return new RedirectResponse($router->generate('list_racks'));

            }

        }else{

            return new RedirectResponse($router->generate('user_home'), 307);

        }


    }

    public function socialLoginCheckAction(Request $request){

    }

}
